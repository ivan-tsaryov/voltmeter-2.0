﻿function DeviceTTSClass(sapi_server_address) {
    this.sapi_server_address = sapi_server_address;
    this.ttsServerQueryText = '';
    this.audioElement = null;

    this.disabled = false;
}

DeviceTTSClass.prototype.BeginTTSAutoplay = function () {
    var closing_sapi_server_address = this.sapi_server_address;
    var closing_ttsServerQueryText = this.ttsServerQueryText
    var closing_audioElement = this.audioElement;

    this.ttsServerQueryText = "";
    $('.tts_autoplay').each(function (index) {
        closing_ttsServerQueryText += ". ";
        var ttsText = $(this).attr('tts_text');
        if (typeof ttsText === typeof undefined || ttsText === false)
            ttsText = $(this).clone().children().remove().end().text();
        closing_ttsServerQueryText += ttsText;
    });

    this.TTS(closing_ttsServerQueryText);
}

DeviceTTSClass.prototype.TTS = function (qtext) {
    if (this.disabled)
        return;

    if (!this.audioElement)
        this.Init();

    this.audioElement.pause();

    var closing_sapi_server_address = this.sapi_server_address;
    var closing_ttsServerQueryText = this.ttsServerQueryText
    var closing_audioElement = this.audioElement;

    var closing_qtext = qtext;

    $('.tts_status_block').text('Синтезатор речи загружается...');
    $('.tts_pause_button').attr('disabled', 'disabled');
    $('.tts_play_button').attr('disabled', 'disabled');
    $('.tts_stop_button').attr('disabled', 'disabled');

    $.ajax({
        type: "post",
        url: closing_sapi_server_address + "/Service1.asmx/GetData",
        contentType: "application/json; charset=utf-8",
        data: '{"text":"' + closing_qtext + '"}',
        dataType: "json",
        success: function (response) {
            var f = response.d;
            if (f.substring(0, 2) == "##") {
                console.log("SERVER ERROR");
                console.log(f);
                $('.tts_status_block').text('Ошибка загруки синтезатора речи');
                return;
            }
            f = f.substring(1, f.length - 1);
            console.log(closing_sapi_server_address + '/' + f);
            closing_audioElement.setAttribute('src', closing_sapi_server_address + '/' + f);
            closing_audioElement.load();
            $('.tts_pause_button').removeAttr('disabled');
            $('.tts_play_button').removeAttr('disabled');
            $('.tts_stop_button').removeAttr('disabled');
            $('.tts_status_block').text('');
        },
        failure: function (response) {
            console(response.d);
            $('.tts_status_block').text('Ошибка загруки синтезатора речи');
        }
    });
}

DeviceTTSClass.prototype.Init = function () {
    if (!this.audioElement) {
        this.audioElement = document.createElement('audio');
        this.audioElement.id = 'audio-tts';
        this.audioElement.autoplay = 'autoplay';
        console.log(this.audioElement);
    }

    var closing_sapi_server_address = this.sapi_server_address;
    var closing_ttsServerQueryText = this.ttsServerQueryText
    var closing_audioElement = this.audioElement;
    var closing_this = this;

    $(".tts_play_button").unbind("click");
    $(".tts_stop_button").unbind("click");
    $(".tts_pause_button").unbind("click");
    $(".tts_switch").unbind("click");

    $('.tts_play_button').click(function () {
        closing_audioElement.pause();
        closing_audioElement.play();
    });
    $('.tts_stop_button').click(function () {
        closing_audioElement.pause();
        closing_audioElement.currentTime = 0;
    });
    $('.tts_pause_button').click(function () {
        closing_audioElement.pause();
    });
    $('.tts_switch').click(function () {
        if (!closing_this.audioElement)
            return;

        if (closing_this.disabled == false)  {
            closing_this.disabled = true
            closing_this.audioElement.pause();
            closing_this.audioElement.currentTime = 0;
        } else {
            closing_this.disabled = false
        }
    });
}

DeviceTTSClass.prototype.AddAutoplayClass = function (foo) {
    foo.first().addClass('tts_autoplay');
}

DeviceTTSClass.prototype.RemoveAllAutoplayClass = function () {
    $('.tts_autoplay').removeClass('tts_autoplay');
}