//Кнопка (имя, параметр отверчает за изменение состояния прибора)
function Button(name, param) {
	Button.superclass.constructor.call(this, name, param);
	this.stateChange = param;
}
extend(Button, Control);

Button.prototype.setMap = function(map) {
	Button.superclass.setMap.call(this, map);
}

//Обработка смены состояние в зависиомти от параметра
//Можно задать строчкой ("d.state = s.state + 1"), выполяется через eval (d - новое состояние, s - старое)
//Массив - последовательное исполнение
//Карта c ключами, cond - условие (строчка), act - действие
Button.prototype.stateChangePerform = function (state, new_state, action) {
	var b = this;
	var map = this.map;
	
	var s = state;
	var d = new_state;
	
	//для упражнений
	d.button_push = this.name;
	if (typeof(action) === 'string') {
		eval(action);
	} 
	else if (action instanceof Array) {
		for(var i = 0; i < action.length; i++) {
			this.stateChangePerform(s, d, action[i]);
		}
	}
		else {
			if (action.act == undefined) {
				this.stateChangePerform(s, d, action.button);
				return;
			}
			var cond = 1;
			if (action.cond != undefined) {
				cond = eval(action.cond);
			}
			
			if (action.fail != undefined) {
				if (cond)
					this.map.stateChangeFail(this, action.fail);
			}
			else
			{
				if (cond) eval(action.act);
			}
		}
}

//Обработка клика
Button.prototype.action = function () {
	this.render.animateClick(this.map);
    this.map.action(this, 'button_click');
    var d = this.map.state
    this.stateChangePerform(d, d, this.stateChange);

    this.map.action(this, 'button_post');

    //this.map.action(this, 'state_change');
    this.map.action(this, 'state_change');
}