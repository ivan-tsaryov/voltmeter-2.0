function CustomErrorBlockReGoal() {}
extend(CustomErrorBlockReGoal, CustomErrorTypeStates);

CustomErrorBlockReGoal.prototype.title = function() {
	return 'Повторное достижение цели';
}

CustomErrorBlockReGoal.prototype.do = function(result) {
	if (this.last_state == null) return;
	var fb = this.map.functionalBlock();
	var block_goal = {};
	var prev_state = null;
	var last_blocks = null;
	var last_multigoal = null;
	for(var key in this.states) {
		var state = this.states[key];

		if (prev_state != null) {
			var blocks = fb.changeState(prev_state, state);
			for(blockname in blocks) {
				var typeblock = blocks[blockname]
				if (parseInt(key) == this.step) {
					if (block_goal[blockname] == 1 && (typeblock == 'goal' || typeblock == 'multigoal')) {
						var e = new CustomError();
						e.type = this;
						e.param = {title: fb.title(blockname)};
						result.push(e);
					}
				}
				else {
					if(typeblock == 'goal')
						block_goal[blockname] = 1;
					if ((last_multigoal != blockname || typeblock == 'change') && block_goal[blockname] == 2) {
						block_goal[blockname] = 1;
					}
					if(typeblock == 'multigoal' && block_goal[blockname] != 1) {
						block_goal[blockname] = 2;
						last_multigoal = blockname;
					}
				}
			}
			last_blocks = blocks;
		}
		prev_state = state;
	}
}

CustomErrorBlockReGoal.prototype.get_text = function(e) {
	return "Ваши действия повторяются, функциональный узел {0} уже задан Вами, необходимо перейти к установке параметров не заданного функционального узла прибора".format(e.param.title);
	// return "Для блока {0} повторно достигнута цель".format(e.param.title);
}