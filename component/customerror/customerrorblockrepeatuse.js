function CustomErrorBlockRepeatUse() {}
extend(CustomErrorBlockRepeatUse, CustomErrorTypeStates);

CustomErrorBlockRepeatUse.prototype.title = function() {
	return 'Повторное использование функционального блока';
}

CustomErrorBlockRepeatUse.prototype.do = function(result) {
	if (this.last_state == null) return;
	var fb = this.map.functionalBlock();
	var block_use = {};
	var prev_state = null;
	var last_blocks = null;
	for(var key in this.states) {
		var state = this.states[key];

		if (prev_state != null) {
			var blocks = fb.changeState(prev_state, state);
			for(blockname in blocks) {
				if (parseInt(key) == this.step) {
					// console.log(fb.title(blockname));
					if (block_use[blockname] == 0) {
						var yet_use = false;
						for(var lastblock in last_blocks) {
							if (lastblock == blockname)
								yet_use = true;
						}
						if (yet_use) continue;
						var e = new CustomError();
						e.type = this;
						e.param = {name: blockname, title: fb.title(blockname)};
						result.push(e);
					}
				}
				else {
					block_use[blockname] = 0;
				}
			}
			last_blocks = blocks;
		}
		prev_state = state;
	}
}

CustomErrorBlockRepeatUse.prototype.get_text = function(e) {
	return 'Установки параметров устройства были уже сделаны ранее, поэтому обратите внимание на ход работы с устройством в инструкции, возможно, настоящие действия является ошибочным '
	return e.param.title;
}