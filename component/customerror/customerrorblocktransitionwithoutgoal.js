function CustomErrorBlockTransitionWithoutGoal() {}
extend(CustomErrorBlockTransitionWithoutGoal, CustomErrorTypeStates);

CustomErrorBlockTransitionWithoutGoal.prototype.title = function() {
	return 'Переход между блоками без достижения цели';
}

CustomErrorBlockTransitionWithoutGoal.prototype.do = function(result) {
	if (this.last_state == null) return;
	if (this.states.length < 3) return;
	var fb = this.map.functionalBlock();
	var sz = this.states.length;
	var prev_blocks = fb.changeState(this.states[sz - 3], this.states[sz - 2]);
	var last_blocks = fb.changeState(this.states[sz - 2], this.states[sz - 1]);

	for(blockname in prev_blocks) {
		if (last_blocks[blockname] == undefined && prev_blocks[blockname] == 'change') {
			var history_block = this.history_block(this.get_states_cut());
			var sz = this.condition_array_equal_size(history_block.reverse(), function(bl) {
				return bl[blockname] != undefined;
			});

			var e = new CustomError();
			e.type = this;
			e.param = {block1: fb.title(blockname), size: sz};
			result.push(e);
		}
	}
}

CustomErrorBlockTransitionWithoutGoal.prototype.get_text = function(e) {
	return "Последние {0} ваших действий были целью установки параметра/ов функционального узла {1}, однако Вы прервали процесс, перейдя на установку параметров следующего функционального узла, тем самым не завершив работу с функциональным узлом {1}".format(e.param.size, e.param.block1);
	return "Для блока {0} не достигнута цель".format(e.param.block1);
}