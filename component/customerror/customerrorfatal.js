function CustomErrorFatal() {}
extend(CustomErrorFatal, CustomErrorType);

CustomErrorFatal.prototype.title = function() {
	return 'Прибор сломался';
}

CustomErrorFatal.prototype.do = function(result) {
	var param = this.last_action['fatal'];
	if (param == undefined) return;

	var e = new CustomError();
	e.type = this;
	e.param = param;
	result.push(e);
}

CustomErrorFatal.prototype.get_text = function(e) {
	return "Ваши действия приводят к поломке прибора, внимательно прочитайте разделы инструкции прибора, касающиеся портов входа и уровня подаваемого напряжения(причина: {0})".format(e.param);
}