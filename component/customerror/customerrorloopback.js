function CustomErrorLoopBack() {}
extend(CustomErrorLoopBack, CustomErrorTypeStates);

CustomErrorLoopBack.prototype.title = function() {
	return 'Петля';
}

CustomErrorLoopBack.prototype.adderror = function(result, size) {
	if (size == 0) return;
	var e = new CustomError();
	e.type = this;
	e.param = {size: size};
	result.push(e);
	return;
}

CustomErrorLoopBack.prototype.do = function(result) {
	if (this.last_state == null) return;
	var states = this.get_states_cut();
	var find = -1;

	for(var key in states.reverse()) {

		if (key == this.step) continue;
		var state = states[key];

		var diff = this.last_state.diff(state);
		diff.minus(this.map.stateTimeKeys());
		if (diff.size() == 0) {
			if (find == -1) find = key;
		}
		else if (find != -1) {
			var size = key - find;
			this.adderror(result, size);
			return;
		}
		else if (key != states.length - 1) {
			return;
		}
	}
	if (find != -1)
		this.adderror(result, key - find);
}

CustomErrorLoopBack.prototype.get_text = function(e) {
	// return "Вы уже были в этом состоянии {0} шагод назад".format(this.step - e.param.step);
	var str = 'Ваши {0} последних действий не приводят к конкретному результату, поэтому определите режим работы прибора и далее действуйте согласно шагам инструкции для установки необходимого режима работы прибора';
	return str.format(e.param.size);
}