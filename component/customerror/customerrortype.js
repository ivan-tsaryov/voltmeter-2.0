function CustomErrorType() {}
CustomErrorType.prototype.init = function(actions, step, map) {
	this.actions = actions;
	this.step = step;
	this.last_action = actions[step];
	this.map = map;
}

CustomErrorType.prototype.filteraction = function(types) {
	var ar = [];
	for(key in this.actions) {
		var a = this.actions[key];
		for(kt in types) {
			var type = types[kt];
			var val = a[type];
			if (val != undefined)
				ar.push(val)
		}
	}
	return ar;
}

CustomErrorType.prototype.get_message = function(e) {
	return '{0}: {1}'.format(this.title(), this.get_text(e));
}

function CustomErrorTypeStates() {}
extend(CustomErrorTypeStates, CustomErrorType);

CustomErrorTypeStates.prototype.condition_array_equal_size = function(states, cond) {
	a = 0;
	for(k in states) {
		st = states[k];
		if (cond(st)) {
			a++;
		} else {
			return a;
		}
	}
	return a;
}

CustomErrorTypeStates.prototype.init = function(actions, step, map) {
	CustomErrorTypeStates.superclass.init.call(this, actions, step, map);
	this.states = this.filteraction(['state']);
	this.last_state = null;
	if (this.last_action['state'] != undefined)
		this.last_state = this.last_action['state'];
	this.functionalBlock =  this.map.functionalBlock();
}

CustomErrorTypeStates.prototype.get_states_cut = function() {
	return this.states.slice(0, -1);
}

CustomErrorTypeStates.prototype.reverse_state_cut = function() {
	return this.states.slice(0, -1).reverse();
}

CustomErrorTypeStates.prototype.history_block = function(states) {
	if (!states) states = this.states;
	var last_st = null;
	var blockes = [];
	for(k in states) {
		var st = states[k];
		if (last_st != null) {
			var fb = this.functionalBlock.changeState(last_st, st);
			blockes.push(fb);
		}
		last_st = st;
	}
	return blockes;
}