function CustomErrorAnalysis(name, param) {
	CustomErrorAnalysis.superclass.constructor.call(this, name, param);
	this.actionhistory = [];
	this.statehistory = [];
	this.erros_list = [];
	this.erros_type = [CustomErrorLoopBack, CustomErrorFatal, CustomErrorBlockRepeatUse, CustomErrorBlockTransitionWithoutGoal, CustomErrorBlockReGoal];
	this.func_log = function(m) {
		console.log(m);
	}
}
extend(CustomErrorAnalysis, Logic);

CustomErrorAnalysis.prototype.setMap = function (map) {
	CustomErrorAnalysis.superclass.setMap.call(this, map);

 	map.addListener('state_change', this, function(control, params) {
 		// console.log(this.map.state);
 		if (params && !params.user) {
 			return;
 		}

 		var last = this.statehistory[this.statehistory.length - 1];
 		var c = new StateDevice(this.map.stateCopy());
 		if (this.statehistory.length == 0 || !c.equal_minus(last, this.map.stateTimeKeys())) {
			this.actionhistory.push({state: c});
			this.statehistory.push(c);
			this.analyze_step(this.actionhistory.length - 1);
		}
	});

	map.addListener('fatal', this, function(control, param) {
		this.actionhistory.push({fatal: param});
		this.analyze_step(this.actionhistory.length - 1);
	});

	map.addListener('mode', this, function(control) {
		this.log_clear();
		this.actionhistory = [];
		this.statehistory = [];
	});

	map.addListener('start', this, function(control) {
		this.log_clear();
		this.actionhistory = [];
		this.statehistory = [];
	});
}

CustomErrorAnalysis.prototype.analyze_step = function(step) {
	var result = [];
	for(var key in this.erros_type) {
		var handler = new this.erros_type[key]();
		handler.init(this.actionhistory, step, this.map);
		handler.do(result);
	}
	for(var key in result) {
		var e = result[key];
		this.func_log(e.get_message());
	}
}