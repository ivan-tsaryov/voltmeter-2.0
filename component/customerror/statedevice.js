//Состояние прибора(обёртка над стандартым хешом, параметр - значение)

//state - стандартое описание состояний для прибора, хеш параметр - значение
function StateDevice(state) {this.state = state;}

//Разница между двумя состояниями
StateDevice.prototype.diff = function(state) {
	return new StateDevice(differenceObject(this.state, state.state));
}
//Кол-во элементов в состоянии
StateDevice.prototype.size = function() {
	return  Object.keys(this.state).length;
}
//Равенство состояний
//state - состояние для сравнения
StateDevice.prototype.equal = function(state) {
	return this.diff(state).size() == 0;
}
//Удаление элеметнов из состояний
//a - массив параметров для удаления
StateDevice.prototype.minus = function(a) {
	for(var key in a) {
		delete this.state[a[key]];
	}
	return this;
}
//Равенство состояний с учётом удалённых параметров
//state - состояние для сравнения
//minus - массив параметров для удаления
StateDevice.prototype.equal_minus = function(state, minus) {
	var dif = this.diff(state);
	dif.minus(minus);
	return dif.size == 0;
}
//Получение значение параметра
//key - параметр
StateDevice.prototype.get = function(key) {
	return this.state[key];
}
//Проверка существования параметра
//key - параметр для проверки
StateDevice.prototype.exist = function(key) {
	return this.state[key] != undefined;
}