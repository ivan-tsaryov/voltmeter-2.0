GeneratorSignal = function(param) {
	this.param = param;
}

GeneratorSignal.prototype.signal = function (nsec) {
	var param = this.param;
	var c = 1000000000
	var p = param.param;
	var t  = Math.floor((1 / p.f) * c);
	var s = nsec / param.scale;
	var k = s / t;
	var signal = s - t * Math.floor(k);
	signal = (signal < p.d * c) * p.v;
	param.funcsignal(nsec, signal);
}

GeneratorOne = function(param) {
	this.param = param;
	this.nsecond = 0;
}

GeneratorOne.prototype.start = function() {
	var interval = 100;
	var scale = this.param.scale;
	var param = this.param;
	var g = this;
	if (this.timer != null) return;
	this.timer = setInterval(function() { 
			
		for(var i = 0; i < param.generator.length; i++) {
			var p = param.generator[i];
			p.signal(g.nsecond);
		}
		g.nsecond += interval * 1000000;
	
	}, interval);
}

GeneratorOne.prototype.stop = function() {
	if (this.timer == null) return;
	clearInterval(this.timer);
	this.timer = null;
}