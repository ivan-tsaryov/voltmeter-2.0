//Картинка
// param.src - путь до картинки
function Image(name, param) {
	Image.superclass.constructor.call(this, name, param);

	var options = {
	    src: ''
  	};

  	this.param = $.extend(options, param);

  	if (this.param.src == '')
  		throw new Error("src is null for Image: " + name);
}
//Наследуется от контроллера
extend(Image, Control)

Image.prototype.defaultOpacity = function() {
	return 1;
}

Image.prototype.get = function() {
	return 1;
}

//Отрисовка контроллера в режиме справки
Image.prototype.drawControl = function () {
	this.draw();
}

Image.prototype.setMap = function (map) {
	Image.superclass.setMap.call(this, map);

	var b = this.box();

	var area = {shape: "image", coords: b.x + "," + b.y + "," + b.width + "," + b.height};

	area.tooltip = this.render.attr.tooltip;
	area.hint_text = this.render.attr.hint_text;

	this.render = new Render(area, this);
	this.render.getNode(map);
	if (this.param.src != 'none')
		this.render.setImage(this.param.src);
}