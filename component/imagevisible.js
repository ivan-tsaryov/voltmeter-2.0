//Двигающиеся картинка (имя, параметры)
// param:
// .src - путь до картинки
// .visible - отображать ли картинку
function ImageVisible(name, param) {
	param = $.extend(this.options, param);

	ImageVisible.superclass.constructor.call(this, name, param);
}

//Наследуется от картинки
extend(ImageVisible, Image)

ImageVisible.prototype.setMap = function (map) {
	ImageVisible.superclass.setMap.call(this, map);

	map.addListener('state_change', this, function() {
		var vars = {t: this.map.state, map: this.map};
		var visible = callFunctionOrEval(this.param.visible, this, vars);
		if (visible)
			this.render.show();
		else
			this.render.hide();
	});
}
