function IndicatorCounter(name, param) {
	IndicatorNumber.superclass.constructor.call(this, name, param);
	this.param = param;
	this.boundsBox = null;
	this.numbers = [];
}
extend(IndicatorCounter, Control)

IndicatorCounter.prototype.setMap = function (map) {
	IndicatorNumber.superclass.setMap.call(this, map);
	map.addListener('state_change', this, function() {
		var cond = this.param.cond;
		var t = map.state;
		if (eval(cond))
			this.setText(eval(this.param.val));
		else
			this.render.hide();
	})
}
	
IndicatorCounter.prototype.setText = function (value) {
	this.render.counterIndicator(value);
}

IndicatorCounter.prototype.draw = function () {
	this.renderControl.hide();
	this.render.renderCounterIndicator(this.map);
	if (this.param.attr) this.render.node.attr(this.param.attr);
}