function IndicatorDb(name, param) {
	IndicatorNumber.superclass.constructor.call(this, name, param);
	this.param = param;
	this.boundsBox = null;
}
extend(IndicatorDb, Control)

IndicatorDb.prototype.setMap = function (map) {
	IndicatorNumber.superclass.setMap.call(this, map);
	map.addListener('state_change', this, function() {
		var cond = this.param.cond;
		var t = map.state;
		if (eval(cond))
			this.setText(eval(this.param.val));
		else
			this.render.hide();
	})
}
	
IndicatorDb.prototype.setText = function (value) {
	this.render.dbIndicator(value);
}

IndicatorDb.prototype.draw = function () {
	this.renderControl.hide();
	this.render.renderDbIndicator(this.map);
	if (this.param.attr) this.render.node.attr(this.param.attr);
}