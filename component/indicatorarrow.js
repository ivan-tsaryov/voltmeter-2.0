//Стрелочный индикатор (Отклоняющиеся стрелочка)
function IndicatorArrow(name, param) {
	IndicatorArrow.superclass.constructor.call(this, name);
	
	this.options = {
	    rotation: {x : 0.5, y: 0.5},  // Центр вращения в процентах
	    length: 0.5, // Длина стрелки относительно диогонали
	    width: 1, // Ширина в пикселях
	    arrowImage: '', // Рисунок стрелки горизонтальный
  	};
	
	this.param = $.extend(this.param, param);
}
//Наследуется от контроллера
extend(IndicatorArrow, Control)

IndicatorArrow.prototype.defaultOpacity = function() {
	return 1;
}

//Навешивается обработчик на  'state_change'
IndicatorArrow.prototype.setMap = function (map) {
	IndicatorArrow.superclass.setMap.call(this, map);

	var b = this.box();
	this.centerX = b.width * this.param.rotation.x + b.x;
	this.centerY = b.height * this.param.rotation.y + b.y;

	var diagonal = Math.sqrt(b.width * b.width + b.height * b.height);
	this.length = this.param.length * diagonal;

	var area = {shape: "image", coords: this.centerX + "," + this.centerY + "," + this.length + "," + this.param.width};

	area.tooltip = this.render.attr.tooltip;
	area.hint_text = this.render.attr.hint_text;

	this.render = new Render(area, this);
	this.render.getNode(map);
	this.render.setImage(this.param.arrowImage, this.length, this.param.width);
	//this.render.getNode(map);
	
	map.addListener('state_change', this, function() {
		var t = this.map.state;
		var controler = this;
		var angle = eval(this.param.val);
		this.setAngle(angle);
	})
}

//Выставление угла
IndicatorArrow.prototype.setAngle = function (angle) {
	this.render.node.transform('r' + (-angle) + ',' + this.centerX + ',' + this.centerY);
	this.angle = angle;
}