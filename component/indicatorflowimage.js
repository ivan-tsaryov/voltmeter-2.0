//Ленточная картинка
function IndicatorFlowImage(name, param) {
	IndicatorFlowImage.superclass.constructor.call(this, name);
	
	this.options = {
	    length: 0, // Длина картинки в пикселях
  	};
	
	this.param = $.extend(this.param, param);
}
//Наследуется от контроллера
extend(IndicatorFlowImage, Control)

IndicatorFlowImage.prototype.defaultOpacity = function() {
	return 1;
}

//Навешивается обработчик на  'state_change'
IndicatorFlowImage.prototype.setMap = function (map) {
	IndicatorFlowImage.superclass.setMap.call(this, map);

	var b = this.box();

	var area = {shape: "image", coords: b.x + "," + b.y + "," + this.param.length + "," + b.height};

	area.tooltip = this.render.attr.tooltip;
	area.hint_text = this.render.attr.hint_text;

	this.render = new Render(area, this);
	this.render.getNode(map);
	this.render.setImage(this.param.image);
	//this.render.getNode(map);
	this.render.node.attr({"clip-rect": '' + b.x + " " + b.y + " " + b.width + " " + b.height});

	map.addListener('state_change', this, function() {
		var t = this.map.state;
		var controler = this;
		var offset = eval(this.param.offset);
		this.setOffset(offset);
	})

}

//Выставление смещения
IndicatorFlowImage.prototype.setOffset = function (val) {
	var b = this.box();
	this.render.node.transform("T" + (this.param.length * val) + ",0")
}