function IndicatorText(name, param) {
	IndicatorText.superclass.constructor.call(this, name, param);
	this.param = param;
}
extend(IndicatorText, Control)

IndicatorText.prototype.setMap = function (map) {
	IndicatorText.superclass.setMap.call(this, map);
	map.addListener('state_change', this, function() {
		var cond = this.param.cond;
		var t = map.state;
		if (eval(cond)) {
			var vars = {t: map.state, map: map};
			var text = callFunctionOrEval(this.param.val, this, vars);
			this.setText(text);
		}
		else
			this.render.hide();
	})
}
	
IndicatorText.prototype.setText = function (text) {
	this.render.text(text);
}

IndicatorText.prototype.draw = function () {
	this.renderControl.hide();
	this.render.renderText(this.map);
	if (this.param.attr) this.render.node.attr(this.param.attr);
}