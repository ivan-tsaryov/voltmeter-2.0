//Базовый класс для контролеров без отображения

//Логический контролер (имя, параметры)
function Logic(name, param) {
	this.name = name;
	this.param = param;
	this.map = null;
}

//Имеет ли внешний вид
Logic.prototype.haveArea = function () {
	return false;
}

//Установка прибора для контроллера
//Необходимо переопределение контролером
//Вызывается прибором, здесь устанавливаются обработчики на прибор
Logic.prototype.setMap = function (map) {
	this.map = map;
}