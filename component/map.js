//Основа прибора
//Свойство
//controls - массив контроллеров
//renderDevice - массив канв
//controls - массив контроллеров
//listener - карта обработчиков событий
//last_state - предыдущее состояние
//send_to_state - состояние для перехода
//mode - режим работы
//actionManager - управляет событиями, при клике передаёь управление нужному контроллеру

//Конструктор (jquery element, ширина, высота)
function Map(node, width, height) {
	this.renderDevice = [];

	if (arguments.length > 0)
		this.addRenderDevice(new RenderDevice(node, width, height));

	this.controls = [];
	this.listener = {};
	this.last_state = null;
	this.send_to_state = null;
	
	this.addListener('state_change', this, this.stateChange);
	this.addListener('fatal', this, this.stateChangeFail);
	//this.addListener('warning', this, this.stateChangeWarning);
	this.mode = deviceMode.EMULATOR;
	this.actionManager = new ActionManager(this);
	this.action_depth = 0;
	this.exercises = [];
	this.startstate = false;
	
	this.addListener('control_action', this, function(source, params) {
		var control = params.control;
		console.log(control.name, control.box());
	})
}

Map.prototype.stateTimeKeys = function() {
	return this.state_time_variables;
}

Map.prototype.addRenderDevice = function (rd) {
	rd.setMap(this);
	this.renderDevice.push(rd);
}

//Установка режима работы (std.js: deviceMode = {EMULATOR: 0, HELP: 1, TRAINING: 2, CONTROL: 3})
Map.prototype.setMode = function (mode) {
	this.mode = mode;
	this.action(this, 'mode');
	this.draw();
}

//Запуск работы прибора
Map.prototype.start = function () {
	this.startstate = true;
	this.action(this, 'start');
}

//Остановка работы прибора
Map.prototype.stop = function () {
	this.startstate = false;
	this.action(this, 'stop');
}

//Сброс прибора (всех состояний)
Map.prototype.restart = function () {
	this.stop();
	this.state = jQuery.extend(true, {}, this.start_state);
	this.start();
	this.draw();
}

//Добавление контроллера в прибор
Map.prototype.add = function (c) {
	this.controls.push(c);
	c.setMap(this);
}


//не используется
Map.prototype.stateChange = function () {
	//console.log(this.state);
}

//Обработка события 'fatal' (вызывается когда прибор ломается, например слишком большое напряжение),
//Выводится сообщение об ошибке и откат состояния
Map.prototype.stateChangeFail = function (control, param) {
	alert('Прибор сломался: ' + param);
	this.rollback();
}

//Обработка события 'warning' (вызывается когда прибор работает некорректно, например неправильно выставлены параметры),
//Выводится сообщение об ошибке
Map.prototype.stateChangeWarning = function (control, param) {
	alert(param);
}

//Попытка откатить состояние
// Не в любой момент состояние прибора можно откатить, поэтому происходят постоянные попытки откатить состояние
Map.prototype.trystateback = function () {
	if (this.action_depth == 0 && this.send_to_state != null) {
		this.state = this.send_to_state;
		this.send_to_state = null;
		this.action(this, "state_change");
	}
}

//Откат состояния на шаг назад
Map.prototype.rollback = function() {
	this.send_to_state = this.last_state;
	this.trystateback();
}

//Откат состояния в заранее сохраненое
Map.prototype.rollbackTo = function(preview_state) { 				// Откат к укзанному состоянию. Троицкий
	this.send_to_state = preview_state;
	this.trystateback();
}

//Выбор контролера по имени (то же самое что и GetControlByName)
Map.prototype.control = function (name) {
	var map = this;
	for(var i = 0; i < this.controls.length; i++ ) {
		var value = this.controls[i];
		if (value.name == name)
			return value;
	}
	return null;
}

// Отрисовка прибора и всех контроллеров
Map.prototype.draw = function (c) {

	var map = this;
	$.each(this.controls, function(index, value) {
		if (value.haveArea())
		{
			if (map.mode == deviceMode.HELP/* || map.mode == deviceMode.TRAINING*/)		//Заменил. Стало много режимов. Отрисовывать синие квадраты надо только в режиме HELP (бывший CONTROLS). Троицкий
				value.drawControl();
			else
				value.draw();
			// switch(map.mode)
			// {
			// 	case deviceMode.EMULATOR:
			// 		value.draw();
			// 	break;
			// 	case deviceMode.HELP:
			// 		value.drawControl();
			// 	break;
			// 	case deviceMode.TRAINING:
			// 		value.draw();
			// 	break;
			// }
		}
	});
	this.action(this, "state_change", {user: false});
}

//Инициализация Упражнений
Map.prototype.InitEx = function() {
}

// Вызов события (источник, название события, параметры)
// На данный момент основные: state_change, fatal, warning
Map.prototype.action = function (control, name, params) {
	var m = this.listener[name];
	if (m === undefined) {
		return;
	}
	this.action_depth ++;
	for(var i = 0; i < m.length; i++) {
		d = m[i];
		d.func.call(d.obj, control, params);
	}
	this.action_depth --;
	
	if (this.action_depth == 0 && name == 'state_change') {
		this.last_state = this.state;
	}
	this.trystateback();
}

//  ДОбавление обработчика сосбытия(название события, объект и функция объекта)
Map.prototype.addListener = function(name, object, func) {
	var m = this.listener[name];
	if (m === undefined) {
		m = [];
		this.listener[name] = m;
	}
	m.push({func : func, obj: object});
}

//Сохранение предыдущего состояния
Map.prototype.state_save = function() {
	this.last_state = this.state;
}

//Создание контролёров по описанию
Map.prototype.renderAreaControl = function() {
	var c = this.definitionControl();
	
	m_a = {}
	for(var n = 0; n < this.renderDevice.length; n++) {
		var rd = this.renderDevice[n];

		var a = rd.getArea();
		for(var i = 0; i < a.length; i++) {
			var area = a[i];
			area.paper = rd.paper;
			area.rd = rd;
			m_a[area.key] = area;
		}
	}
	
	for(var i = 0; i < c.length; i++) {
		var controldef = c[i];
		var control = new controldef.cls(controldef.key, controldef.param);
		if (control.haveArea()) {
			var area = m_a[controldef.key];
			if (area === undefined){
				//console.log(controldef);
				throw new Error("not found area: " + controldef.key);
			}
			control.setPaper(area.paper);
			control.renderDevice = area.rd;
			var render = new Render(area, control);
			control.setRender(render);
		}
		this.add(control);
	}
}

//Для отладки
Map.prototype.logControl = function() {
	var a = this.getArea();
	
	m_a = {}
	for(var i = 0; i < a.length; i++) {
		var area = a[i];
		m_a[area.key] = area;
	}
	
	var s = ""
	for(var i = 0; i < this.controls.length; i++) {
		var control = this.controls[i];
		var t = "";
		if (m_a[control.name] != undefined)
			t = m_a[control.name].tooltip;
		s += control.name + "\t" + control.constructor.name + "\t" + t + "\n";
	}
	console.log(s);
}

//Выбор контролера по имени (то же самое что и control)
Map.prototype.GetControlByName = function(name) {		// функция для получения доступа к компонентам map. Троицкий
	var i = 0;
	for (; i < this.controls.length; i++) {
		if (this.controls[i].name == name)
			return this.controls[i];
	}
}

Map.prototype.GetExList = function() {				// получить список упражнений для данного map. Троицкий
	var foo = [];
	for (var i = 0; i < this.exercises.length; i++) {
		foo.push(this.exercises[i].name);
	}
	return foo;
}

 Map.prototype.GetEx = function(num) {				// получить упражнение по номеру. Троицкий
 	return this.exercises[num];
 }


Map.prototype.differenceArray = function(state1, state2) {
	if (state1.length != state2.length) return false;
	for(var i = 0; i < state1.length; i++) {
		if (state1[i] != state2[i]) return false;
	}
	return true;
}

Map.prototype.differenceObject = function(state1, state2) {
	var diff = {};
	for (key in state1) {
		var val = state1[key];
		var val2 = state2[key];

		if (typeof(val) != typeof(val2))
			diff[key] = val2;
		else if ((val instanceof Array) && (val2 instanceof Array)) {
			if (!this.differenceArray(val, val2))
				diff[key] = val2;
		}
		else if (val != val2)
			diff[key] = val2;
	}
	return diff;
}

 Map.prototype.differenceState = function(state) {
 	return this.differenceObject(this.state, state);
 }

Map.prototype.changeState = function (state, source) {
	this.state = $.extend(this.state, state);
	this.action(source, 'state_change');	
}

Map.prototype.stateCopy = function () {
	var d = jQuery.extend(true, {}, this.state);
	return d;
}