//����������
//��������� � ������� flotcharts.org
//name - ��� �����������
//parm - ��������� (container - jquery element-��������� ��� �����������; options - ��������������� ���������� �������, ������ �������� � ������� $.plot; advoptions - ��������������� ���������� ����������)
//advoptions: scaleX/Y - ��������� �� ����� X/Y,  moveX/Y - ����������� �� ����� X/Y
function Oscilloscope(name, param){
	GraphVisio.superclass.constructor.call(this, name, param);
	this.data = [];
}
extend(Oscilloscope, Logic);

//��������� �������
//timesignal - ����� �������
//signal - ������� �������
Oscilloscope.prototype.signal = function(timesignal, signal) {
	//this.timesignal = timesignal;
	if (this.data.length >= this.advopt.max) {
		this.data = this.data.slice(1);
	}		
	this.data.push([timesignal, signal]);
	this.refresh();
}

//����������� �������, ����� ������� signal ���������� ������������� 
Oscilloscope.prototype.refresh = function() {
	var res = [];
	var last = 0;
	for (var i = 0; i < this.data.length; ++i) {
		var val = this.data[i][1];
		var time = this.data[i][0];
		if (last > 0 && val == 0 && i > 0) 
			res.push([time, last]);
		if (last == 0 && val > 0 && i > 0) 
			res.push([time, last]);
		res.push([time, val])
		last = val;
	}	
	res = this.transform(res);
	var series = [{data: res, /*lines: {fill: true},*/ color: "#33CC66"}];
	this.plot.setData(series);
	this.plot.draw();
}

Oscilloscope.prototype.transform = function(series) {
	var transformseries = [];
	for (var i = 0; i < series.length; i++) {
		var a = series[i];
		var x = a[0];
		var y = a[1];
		transformseries.push([this.transformX(x), this.transformY(y)]);
	}
	return transformseries;
}

Oscilloscope.prototype.transformX = function(value) {
	return this.advopt.scaleX * (value + this.advopt.moveX);
}

Oscilloscope.prototype.reversetransformX = function(value) {
	return value / this.advopt.scaleX - this.advopt.moveX;
}

Oscilloscope.prototype.transformY = function(value) {
	return this.advopt.scaleY * (value + this.advopt.moveY);
}

Oscilloscope.prototype.reversetransformY = function(value) {
	return value / this.advopt.scaleY - this.advopt.moveY;
}

//���������� ����������� � ������
Oscilloscope.prototype.setMap = function (map) {
	Oscilloscope.superclass.setMap.call(this, map);
	this.container = this.param.container;
	var series = [{data: [], lines: {fill: true}, color: "black"}];
	
	var defadvopt = {
		max: 30,
		scaleX: 1,
		scaleY: 1,
		moveX: 0,
		moveY: 0
	};
	this.advopt = $.extend(true, defadvopt, this.param.advoptions);
	
	var g = this;
	var opt = {
		grid: {	borderWidth: 1,	minBorderMargin: 20,labelMargin: 10,
			backgroundColor: { 	colors: ["#E6E6FA", "#E6E6FA"]	},
			margin: { top: 8, bottom: 20, left: 20}
			/*,
			markings: function(axes) {
				var markings = [];
				var xaxis = axes.xaxis;
				for (var x = Math.floor(xaxis.min); x < xaxis.max; x += xaxis.tickSize * 2) {
					markings.push({ xaxis: { from: x, to: x + xaxis.tickSize }, color: "rgba(232, 232, 255, 0.2)" });
				}
				return markings;
			}*/
		},
		xaxis: {
			min: 0,
			max: g.advopt.max,
			tickFormatter: function (val, axis) {return g.reversetransformX(val);}
		},
		yaxis: {
			min: 0,
			max: 10000,
			tickFormatter: function (val, axis) {return g.reversetransformY(val);}
		},
		legend: {
			show: true
		}
	}
	
	//, xaxes: [{}]
	opt = $.extend(true, opt, this.param.options);
	
	
	this.plot = $.plot(this.container, series, opt);
}