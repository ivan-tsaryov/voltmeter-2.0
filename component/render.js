//Рендер, функции вывзваеются контроллером в зависимости от того, что он хочет вывести
//node - Raphael node, распалагающиеся в координатах контроллера и отображающая его
//Если в будующем заменим Raphael, поменять практически только здесь


//Рендер атрибуты и контролер, attr.coords - кординаты в строчку через запятую
function Render(attr, control) {
	this.node = null;
	this.attr = attr;
	this.control = control;
}

//Канва
Render.prototype.paper = function() {
	return this.control.paper;
}

//Анимация клика по контроллеру
Render.prototype.animateClick = function(map) {
	var bb = this.node.getBBox();
	var flush = this.paper().circle(bb.x + bb.width / 2, bb.y + bb.height / 2, Math.min(bb.width, bb.height) / 2).attr("fill", "red");
	flush.animate({r: 0}, 300, function() {flush.remove()});
}

//Удаление
Render.prototype.remove = function() {
	var bb = this.node.remove();
}

Render.prototype.setClipRect = function (left, top, width, height) {
	this.node.attr({"clip-rect": '' + left + " " + top + " " + width + " " + height})
}

//Установка картинки
Render.prototype.setImage = function(src, w, h) {
	if (typeof(w) == "undefined") {
		this.node.attr({src: src});
		return;
	}
	this.node.attr({src: src, width: w, height: h});
}

//Светоиндикация вкл
Render.prototype.lightOn = function(map) {
	this.node.attr({fill: 'yellow', opacity: '100'});
}

//Светоиндикация вкл с прозрачностью
Render.prototype.lightOnOpacity = function(n) {
	this.node.attr({fill: 'yellow', opacity: n});
}

//Светоиндикация выкл
Render.prototype.lightOff = function(map) {
	this.node.attr({fill: 'white', opacity: '0'});
}

//Получение координат
Render.prototype.getCoordsRect = function() {
	if (typeof(this.attr.coords) !== 'string') {
		return this.attr.coords;
	} 
	var c = this.attr.coords.split(',');
	var x = parseInt(c[0]);
	var y = parseInt(c[1]);
	var x2 = parseInt(c[2]);
	var y2 = parseInt(c[3]);
	return {left: x, top: y, width: x2 - x, height: y2 - y};
}

//Получение координат для окружности
Render.prototype.getCoordsCircle = function() {
	if (typeof(this.attr.coords) !== 'string') {
		return this.attr.coords;
	} 
	var c = this.attr.coords.split(',');
	x = parseInt(c[0]);
	y = parseInt(c[1]);
	r = parseInt(c[2]);
	return {left: x, top: y, radius: r};
}

//Создание ноды
Render.prototype.renderNumber = function(map, attr) {
	if (this.node == null) {
		var c = this.getCoordsRect();
		var x = Math.round(c.left + c.width / 2);
		var y = Math.round(c.top + c.height / 2);

		this.node = this.paper().text(x, y);
	}
	this.hide();
	if (attr == null)
		attr = {fill: 'green', 'font-size': 38};
	this.node.attr(attr);
}

//Вывод числа 
Render.prototype.number = function(n, int, fract) {
	this.node.attr('text', zeroFill(n, int, fract));
	this.node.show();
}

//Создание ноды 
Render.prototype.renderText = function(map) {
	if (this.node == null) {
		var c = this.getCoordsRect();
		var x = Math.round(c.left + c.width / 2);
		var y = Math.round(c.top + c.height / 2);
		this.node = this.paper().text(x, y);
	}
	this.hide();
	this.node.attr({'fill': this.attr.fill, 'font-size': this.attr.font_size, 'font-family' : this.attr.font_family});
}

//Вывод текста
Render.prototype.text = function(text) {
	this.node.attr('text', text);
	this.node.show();
}

//Создание ноды(или поучение, если уже создана), в зависимочти от атрибутов (this.attr) (кружок, прямоугольник, картинка)
Render.prototype.getNode = function(map) {
	if (this.node != null) return this.node;
	switch (this.attr.shape)
	{
		case 'rect': 
			var c = this.getCoordsRect();
			this.node = this.paper().rect(c.left, c.top, c.width, c.height);
			$(this.node.node).attr('title', this.attr.tooltip);		//Добавляю строку-подсказку к каждому svg-элементу. Троицкий
			$(this.node.node).attr('hint_text', this.attr.hint_text);		//Добавляю текст для режима справки к каждому svg-элементу. Троицкий
			//$(this.node.node).attr('class', 'control');
		break;
		case 'circle': 
			var c = this.getCoordsCircle();
			this.node = this.paper().circle(c.left, c.top, c.radius).attr({fill: '#000', opacity: '0'});
			$(this.node.node).attr('title', this.attr.tooltip);		//Добавляю строку-подсказку к каждому svg-элементу. Троицкий
			$(this.node.node).attr('hint_text', this.attr.hint_text);		//Добавляю текст для режима справки к каждому svg-элементу. Троицкий
		break;
		
		case 'image': 
			c = this.attr.coords.split(',');
			x = c[0];
			y = c[1];
			w = c[2];
			h = c[3];
			this.node = this.paper().image('', x, y, w, h);
			$(this.node.node).attr('title', this.attr.tooltip);		//Добавляю строку-подсказку к каждому svg-элементу. Троицкий
			$(this.node.node).attr('hint_text', this.attr.hint_text);		//Добавляю текст для режима справки к каждому
		break;
	}
	var control = this.control;
	//Обработка клика
	this.node.click(function() {
		map.actionManager.click(control);
	});
	this.node.mouseover(function() {			// добавил событие "мышка над элементом".Троицкий
		map.actionManager.mouseover(control, this.node);
	});
	this.node.mouseout(function() {			// добавил событие "мышка над элементом".Троицкий
		map.actionManager.mouseout(control, this.node);
	});
	return this.node;
}

// Рендер контроллёра
Render.prototype.render = function(map) {
	this.getNode(map);
	var opacity = this.control.defaultOpacity();
	this.node.attr({fill: '#000', opacity: opacity});
}

// Рендер контроллёра в режиме справки
Render.prototype.renderControl = function(map) {
	this.getNode(map);
	this.node.attr({fill: 'blue', opacity: '0.3', stroke: 'red'});
}

// Скрыть
Render.prototype.hide = function() {
	if (this.node != null) this.node.hide();
}

//Показать
Render.prototype.show = function() {
	if (this.node != null) this.node.show();
}

//Устаноувка прозрачности
Render.prototype.opacity = function(n) {
	this.node.attr({opacity: n});
}

//Подсветить контролер вкл
Render.prototype.highlightControl = function() {
	this.node.attr({fill: 'cyan', opacity: '0.6', stroke: 'green'});
}

//Подсветить контролер выкл
Render.prototype.highlightControlOff = function() {
	this.node.attr({fill: 'blue', opacity: '0.3', stroke: 'red'});
}