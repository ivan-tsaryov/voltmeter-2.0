//Вращающаяся картинка (для крутилок)
//Обработка разбита на две части, отпредение угла врещение и прорисовка это угла
//Прорисовка реализована через обработку изменения состояния прибора
//Шаги для 1 этапа (изменение угла):
//1) Двигаем мышкой
//2) Опредеяем угол вращения ручки
//3) Изменяем состояние прибра
//Шаги для 2 этапа (Отрисовка):
//1) Слушаем сообщение об измении состоянии прибора
//2) Определяем угол вращения
//3) Отрисовываем угол

//param.action = строчка вызываемая через eval при вращении,
// 	локльные переменные map - прибор, val - угол, d - состояние прибора для изменение, пример {action: 'd.handle_angle = val'}
//param.angle = строчка вызываемая через eval для установления угла поворота
// 	локльные переменные controler - контролер, t - состояние прибора, пример {angle: 't.handle_angle'}
function RoundImage(name, param) {
	RoundImage.superclass.constructor.call(this, name, param);

	this.options = {
	    rotation: {x : 0.5, y: 0.5},  // Центр вращения в процентах
	    rounds: false, // включение множества оборотов 
	    		      //(Позволяет сделать несолько кругов ручкой, в результате например 
	    		      // будет вращение на 370 градусов, один круг и 10 градусов, иначае просто 10 градусов )
		moveonclick: true //Вращение по клику, иначе только по движению
  	};
	
	this.param = $.extend(this.param, param);


	this.deg2rad = Math.PI / 180;
	this.rad2deg = 180 / Math.PI;
	this.captured = false;
	this.capuredMouseAngle = 0;
	this.oldDx = 0;
	this.oldDy = 0;
}
extend(RoundImage, Control);

//Контролер отображается
RoundImage.prototype.defaultOpacity = function() {
	return 1;
}


//JQuery элемент ручки
RoundImage.prototype.getjnode = function () {
	return $(this.renderControl.node.node);
}

//Центр вращение относительно документа
RoundImage.prototype.centerRotation = function () {
	//var offset = this.getjnode().offset();
	var offset = $(this.renderDevice.node).offset();
	var b = this.box();
	return {x: offset.left + this.centerX + b.x, y: offset.top + this.centerY + b.y};
}

RoundImage.prototype.setMap = function (map) {
	RoundImage.superclass.setMap.call(this, map);
	
	var b = this.box();

	this.centerX = b.width * this.param.rotation.x;
	this.centerY = b.height * this.param.rotation.y;

	this.centerAbsX = this.centerX + b.x;
	this.centerAbsY = this.centerY + b.y;

	var area = {shape: "image", coords: b.x + "," + b.y + "," + b.width + "," + b.height};
	area.tooltip = this.render.attr.tooltip;
	area.hint_text = this.render.attr.hint_text;
	if (this.param.top) {
		area.coords = (b.x + b.width/2)  + "," + b.y + ",5";
	}
	this.render = new Render(area, this);
	this.render.getNode(map);
	this.render.setImage(this.param.image, b.width, b.height);
	//this.render.getNode(map);

	this.centerRotationVal = this.centerRotation();

	map.addListener('state_change', this, function() {
		var t = this.map.state;
		var controler = this;
		var angle = eval(this.param.angle);
		this.drawAngle(angle);
	});

	map.addListener('flush_control', this, function() {
		this.releaseMouse();
	});


	var g = this;
	$(document).bind('mousemove', function(e) {g.updateAngle(e)});
    $(document).bind('mouseup', function(e) {g.releaseMouse(e)});
	this.render.node.mousedown(function(e) {g.captureMouse(e)});

}

//Отрисовка контроллера в режиме справки
RoundImage.prototype.drawControl = function () {
	//Control.prototype.drawControl.call(this);
	this.renderControl.renderControl(this.map);
	this.renderControl.node.toFront();
	this.renderControl.show();
}

//Захват мыши
RoundImage.prototype.captureMouse = function(e){
	if (this.captured)
		return;
	this.captured = true;

	var mouseAngle = this.getMouseAngle(e);
	this.capuredMouseAngle = mouseAngle;
	this.capuredAngle = this.angle;
	var angle = this.processAngle(mouseAngle);
	this.setAngle(angle);
}

//Обновление угла вращения
RoundImage.prototype.updateAngle = function(e){
	if (this.captured){
	  var mouseAngle = this.getAngle(e);
	  this.setAngle(mouseAngle);
	}
}

//Отпускание мыши
RoundImage.prototype.releaseMouse = function(){
	this.captured = false;
}

//Вызов события обновления состояния ручки
RoundImage.prototype.update = function(val) {
	var s = this.map.state
	var d = jQuery.extend(true, {}, s);
	var map = this.map;
	eval(this.param.action);
	this.map.state = d;
	
	this.map.action(this, 'state_change');
}

//Отрисовка угла
RoundImage.prototype.drawAngle = function(a) {
	this.render.node.transform('r' + (-a) + ',' + this.centerAbsX + ',' + this.centerAbsY);
	this.angle = a;
}

//Уствнока угла
RoundImage.prototype.setAngle = function(a) {
	this.update(a);
}

//Деление на цело
RoundImage.prototype.div = function(val, by){
	return (val - val % by) / by;
}

 // Разница между углами
RoundImage.prototype.diffAngle = function(a, b) {
	a %= 360;
	b %= 360;
	var d = (a - b)  % 360;
	if (Math.abs(d) > 180) {
		if (d > 0) 
			d = d - 360;
		else
			d = 360 + d;
		
	}
	return d;
 }

RoundImage.prototype.processAngle = function(mouseAngle) {
	if (!this.param.moveonclick) {
		var delta = this.diffAngle(mouseAngle, this.capuredMouseAngle);
		mouseAngle = this.normalizeAngle(this.capuredAngle + delta);
	}

	if (!this.param.rounds)
		return mouseAngle;


	var old = this.angle;
	var delta = this.diffAngle(mouseAngle, old);
	
	var angle = this.angle + delta;
	//console.log(old, mouseAngle, delta, angle);
	return angle;
}

RoundImage.prototype.getAngle = function(event) {
	var mouseAngle = this.getMouseAngle(event);
	return this.processAngle(mouseAngle);
}

RoundImage.prototype.normalizeAngle = function(angle){
	if (angle < 0) {
	  var n = this.div(-angle, 360);
	  angle += (n + 1) * 360;
	}
	  
	if (angle > 360)
		angle = angle % 360;

	return angle;
}

//Вычесление угла наклона по положению мыши
RoundImage.prototype.getMouseAngle = function(event){
	var cr = this.centerRotation();
	var mouseLeft = event.pageX - cr.x + 0.1;
	var mouseTop = cr.y - event.pageY;


	var angle = Math.atan(mouseTop / mouseLeft) * this.rad2deg;

	if (mouseLeft < 0)
	  angle += 180;

	return this.normalizeAngle(angle);
}

//Перемещение
RoundImage.prototype.transfer = function(dx, dy){
	this.centerRotationVal.x += dx - this.oldDx;
	this.centerRotationVal.y += dy - this.oldDy;

	this.centerAbsX += dx - this.oldDx;
	this.centerAbsY += dy - this.oldDy;
	

	var b = this.box();
	this.render.node.attr({x: b.x + dx, y: b.y + dy});
	this.oldDx = dx;
	this.oldDy = dy;
}
