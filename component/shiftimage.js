//Смена Картинки
function ShiftImage(name, param) {
	param.src = 'none';
	ShiftImage.superclass.constructor.call(this, name, param);
}
//Наследуется от картинки
extend(ShiftImage, Image)

ShiftImage.prototype.getSrc = function() {
	var vars = {t: this.map.state, map: this.map};
	return callFunctionOrEval(this.param.srcfunc, this, vars)
}

ShiftImage.prototype.setMap = function (map) {
	ShiftImage.superclass.setMap.call(this, map);

	map.addListener('state_change', this, function() {

		var src = this.getSrc();
		this.render.setImage(src);
	});
}