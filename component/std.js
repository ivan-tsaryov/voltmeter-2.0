//Типа наследование классов
function extend(Child, Parent) {
	var F = function() { }
	F.prototype = Parent.prototype
	Child.prototype = new F()
	Child.prototype.constructor = Child
	Child.superclass = Parent.prototype
}

// вспомогательная функция. распечатка в стиле print_r. Троицкий
function print_object(obj, condition) {
	var s = "";
	var foo = "";
	for (var prop in obj) {
   		if (typeof obj[prop] == "function")
			continue;
		if (!(condition === undefined))
			if (condition(prop))
				continue;
		if (typeof obj[prop] == "object")
			foo = print_object(obj[prop]);
		else
			foo = obj[prop];
   	    s += prop + ": " + foo + ";\n";
	}
	return s;
}

function Add_CSS(css_file) {
	$("head").append("<link>");
	css = $("head").children(":last");
	css.attr({
		rel:  "stylesheet",
		type: "text/css",
		href: css_file
	});
}

// Новый режим Обучение и контроль. Троицкий
var deviceMode = {EMULATOR: 0, HELP: 1, TRAINING: 2, CONTROL: 3}

//Сервер результатов тестирования
//var mainserver = "http://localhost:9000/moodle/";
var mainserver = "http://umz.ukoo.ru/";

//Вывод результатов тестирования в виде таблицы
function teststat(device) {
	var column_name = ["id", "name", "soname", "testname", "updated", "value"];
	var column_title = ["№", 'Имя', 'Фамилия', 'Тест', 'Дата', "Балл"];
	var d = [];
	var name = device.constructor.name;
	var container = $(document.createElement("div"));
	container.attr('title', 'Статистика для прибора: ' + device.title);
	container.css('width', '100%');
	var table = $(document.createElement("div"));
	table.css("font-size", "14px");
	container.append(table);
	$.getJSON(mainserver + "report/deviceinfo/?callback=?", {type: 'select', device: name}, function( data ) {
		$(data).each(function(index, element){
			var e = [];
			$(column_name).each(function(index, h){
				var l = element[h];
				if (h == "updated") {
					var dt = moment.unix(parseInt(element[h]));
					moment.lang('ru');
					l = dt.format('DD MMM YYYY, h:mm:ss');
				}
				e.push(l);
			});
			d.push(e);
		});
					
		table.empty();
		table.TidyTable({
				columnTitles : column_title,
				columnValues : []
			});
		table.TidyTable({
				columnTitles : column_title,
				columnValues : d
			});
		container.dialog({
			width:'90%',
			modal: true,
			dialogClass: 'withclose'
		});
	});
}

//Копирование объектов
function cp(o) {
	return jQuery.extend(true, {}, o);
}

function log10(val) {
  return Math.log(val) / Math.LN10;
}

function callFunctionOrEval(val, context, localVar) {
	if (typeof(val) === 'string') {
		with (localVar) {
			return eval(val);
		}
	} 
	return val.call(context.map, context);
}

function callFunctionOrEvalPro(func, context, vars) {
	if (typeof(func) === 'string') {
		with (vars) {
			return eval(func);
		}
	} 
	if (typeof(func) === 'number') {
		return func;
	}
	return func.call(context, vars);
}

$.fn.scrollTo = function( target, options, callback ){
  if(typeof options == 'function' && arguments.length == 2){ callback = options; options = target; }
  var settings = $.extend({
    scrollTarget  : target,
    offsetTop     : 50,
    duration      : 500,
    easing        : 'swing'
  }, options);
  return this.each(function(){
    var scrollPane = $(this);
    var scrollTarget = (typeof settings.scrollTarget == "number") ? settings.scrollTarget : $(settings.scrollTarget);
    var scrollY = (typeof scrollTarget == "number") ? scrollTarget : scrollTarget.offset().top + scrollPane.scrollTop() - parseInt(settings.offsetTop);
    scrollPane.animate({scrollTop : scrollY }, parseInt(settings.duration), settings.easing, function(){
      if (typeof callback == 'function') { callback.call(this); }
    });
  });
}

objectsize = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

function differenceArray(state1, state2) {
  if (state1.length != state2.length) return false;
  for(var i = 0; i < state1.length; i++) {
    var diff = differenceObject(state1[i], state2[i]);
    if (Object.keys(diff).length != 0) return false;
  }
  return true;
}

function differenceObject(state1, state2) {
  var diff = {};
  for (var key in state1) {
    var val = state1[key];
    var val2 = state2[key];

    if (typeof(val) != typeof(val2))
      diff[key] = val2;
    else if (val instanceof Array) {
      if (!differenceArray(val, val2)) diff[key] = val2;
  	}
    else if (typeof(val) == 'object') {
    	var d = differenceObject(val, val2);
    	if (Object.keys(d).length != 0) diff[key] = val2;
    }
    else if (val != val2)
      diff[key] = val2;
  }
  return diff;
}