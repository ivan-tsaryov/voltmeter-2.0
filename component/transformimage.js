//Двигающиеся картинка (имя, параметры)
// param:
// .src - путь до картинки
// .offset - функция которая возвращает смещение в  формате {x: смещение по X в пикселях, y: смещение по Y в пикселях}
function TransformImage(name, param) {

	this.options = {
	    flowX: true // Прокрутка вдоль оси X
  	};
	
	param = $.extend(this.options, param);

	TransformImage.superclass.constructor.call(this, name, param);
}

//Наследуется от картинки
extend(TransformImage, Image)

TransformImage.prototype.isFlowImge = function () {
	return this.param.length != null;
}

TransformImage.prototype.setMap = function (map) {
	TransformImage.superclass.setMap.call(this, map);

	if (this.isFlowImge()) {
		var b = this.box();
		if (this.param.flowX)
			this.render.setImage(this.param.src, this.param.length, b.height);
		else
			this.render.setImage(this.param.src, b.width, this.param.length);
		this.render.setClipRect(b.x, b.y, b.width, b.height);
	}

	map.addListener('state_change', this, function() {
		var vars = {t: this.map.state, map: this.map};
		if (this.param.offset != undefined) {
			var offset = callFunctionOrEval(this.param.offset, this, vars);
			this.setOffset(offset);
		}
		if (this.param.tranform != undefined) {
			var tranform = callFunctionOrEval(this.param.tranform, this, vars);
			this.render.node.transform(tranform);
		}
	});
}

//Отрисовка контроллера в режиме справки
TransformImage.prototype.drawControl = function () {
	//Control.prototype.drawControl.call(this);
	this.renderControl.renderControl(this.map);
	this.setOffsetNode(this.renderControl.node, this.offset);
	this.renderControl.node.toFront();
	this.renderControl.show();
}

TransformImage.prototype.setOffsetNode = function (node, offset) {
	if (offset == undefined)
		return;
	var flow = this.param.flowX ? offset.x : offset.y;
	if (this.isFlowImge()) {
		var b = this.box();
		//this.render.setClipRect(b.x + offset.x, b.y + offset.y, b.width, b.height);
		//flow += this.param.length * offset.flow;
	}
	
	if (this.param.flowX)
		node.transform("T" + flow + "," + offset.y);
	else
		node.transform("T" + offset.x + "," + flow);
}


TransformImage.prototype.setOffset = function (offset) {
	this.offset = offset;
	var flow = this.param.flowX ? offset.x : offset.y;
	if (this.isFlowImge()) {
		var b = this.box();
		this.render.setClipRect(b.x + offset.x, b.y + offset.y, b.width, b.height);
		flow += this.param.length * offset.flow;
	}
	
	if (this.param.flowX)
		this.render.node.transform("T" + flow + "," + offset.y);
	else
		this.render.node.transform("T" + offset.x + "," + flow);
}