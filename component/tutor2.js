var ExerciseType = {STATE_DISCRETE: 0, STATE_CONTINUOUS: 1, INPUT: 2}; //Типы упражнений

Tutor = function (name, param) {

	this.options = {
	    root: ''  // Адрес корня
  	};
	param = $.extend(this.options, param);

	Tutor.superclass.constructor.call(this, name, param);

	this.ex = {};
	this.fail_count = 0;
	this.step = 0;
	this.last_state = {};

	this.user_firstName = "";
	this.user_secondName = "";
	this.stop = true;

	Add_CSS(this.param.root + "tutor/tutor2.css");
}

extend(Tutor, Logic);

Tutor.prototype.validate_STATE_CONTINUOUS = function (node) {
	if (node.state_changed == '*')
		return true;
	var dif = this.map.differenceState(this.last_state);
	for(key in dif) {
		if ($.inArray(key, node.state_changed) == -1) {
			this.error(true);
			return false;
		}
	}
	return true;
}

Tutor.prototype.validate_INPUT = function (node) {
	if (this.nullStateChange()) 
		return true;

	if (node.state_changed == undefined){
		this.error(true);
		return false;
	}

	if (node.state_changed.length == 1 &&  node.state_changed == '*'){
		return true;
	}

	var dif = this.map.differenceState(this.last_state);
	for(key in dif) {
		if ($.inArray(key, node.state_changed) == -1) {
			this.error(true);
			return false;
		}
	}
	return true;
}

Tutor.prototype.validate_STATE_DISCRETE = function (node) {
	if (this.nullStateChange()) 
		return true;
	
	if (node.test != undefined) {
		var res = node.test.call(this.map, {node: node});
		if (!res) {
			this.error(true);
			return false;
		}
		this.setNextGoal();
		return true;
	}

	var standard = node.state;
	for (key in standard) {
		if (standard[key] != this.map.state[key]) {
			this.error(true);
			return false;
		}
	}
	this.setNextGoal();
	return true;
}

Tutor.prototype.nullStateChange = function () {
	var dif = this.map.differenceState(this.last_state);
	if (Object.keys(dif).length == 0) 
		return true;
	return false;
}

Tutor.prototype.setMap = function (map) {
	Tutor.superclass.setMap.call(this, map);
 	//this.showTutorDialog();

 	map.addListener('state_change', this, function(control) {
		if (this.map.mode != deviceMode.TRAINING && this.map.mode != deviceMode.CONTROL)
			return;
		
		if (this.ex === undefined)
			return;

		var node = this.ex.nodes[this.step];
		if (node.type == ExerciseType.STATE_CONTINUOUS) {
			if (!this.validate_STATE_CONTINUOUS(node))
				return;
		}
		if (node.type == ExerciseType.STATE_DISCRETE) {
			if (!this.validate_STATE_DISCRETE(node))
				return;
		}
		if (node.type == ExerciseType.INPUT) {
			if (!this.validate_INPUT(node))
				return;
		}
			
		this.last_state = this.map.stateCopy();
		
	});
}

Tutor.prototype.showTutorDialog = function() {
	if ($('#tutorDialog').length == 0)
		this.createDialog();
	var foo = this;
    $( "#tutorDialog" ).dialog({
      draggable: false,
      closeOnEscape: false,
      autoOpen: false,
      height: 300,
      width: 350,
      modal: true,
      buttons: {
        "ОК": function() {
        	foo.user_firstName = $('#firstName').val();
        	foo.user_secondName = $('#secondName').val();
            $( this ).dialog( "close" );
        }
      },
      close: function() {
      }
    });
	$( "#tutorDialog" ).dialog( "open" );
}

Tutor.prototype.createDialog = function() {
	$('body').append( "<div id='tutorDialog' title='Запрос системы'>" +
					  "  <p class='validateTips'>Введите свои данные</p>" +
					  "  <form>" +
					  "    <table>" +
					  "      <tr><td><label for='firstName'>Фамилия</label></td>" +
					  "      <td><input type='text' name='firstName' id='firstName' class='ui-widget-content ui-corner-all' /></td></tr>" +
					  "      <tr><td><label for='secondName'>Имя</label></td>" +
					  "      <td><input type='text' name='secondName' id='secondName' class='ui-widget-content ui-corner-all' /></td></tr>" +
					  "    </table>" +
					  "  </form>" +
					  "</div>");
}


Tutor.prototype.loadExercise = function(ex) {
	if (this.map.mode == deviceMode.CONTROL)
		this.map.control('menu').LockMenuItems();
	this.map.restart();
	this.fail_count = 0;

	this.ex = ex;
	if (ex.code != undefined) {
		eval(ex.code);
	}

	this.makeMenuItem();
	this.step  = -1;
	this.setNextGoal();
}

Tutor.prototype.state_change = function(state) {
	this.map.state = $.extend(this.map.state, state);
	this.map.action(this, 'state_change');
}

Tutor.prototype.error = function(abort) {
	this.map.action(this, 'flush_control');	
	switch(this.map.mode) {
		case deviceMode.TRAINING:
			alert("Ошибка!");
			break;
		case deviceMode.CONTROL:
			this.fail_count++;
			console.log(this.fail_count);
			if (this.fail_count >= 6)
				this.Abort();
			else
				alert("Ошибка!");
			break;
	}
	if (abort)
		this.map.rollbackTo(this.last_state);
}

Tutor.prototype.inputValue = function() {
	var node = this.ex.nodes[this.step];
	var res = {};
	switch(node.type)
	{
		case ExerciseType.STATE_CONTINUOUS:
			if (node.test != undefined) {
				res = node.test.call(this.map, {node: node});
			}
			else {
				var val = prompt('Введите значение');
				if (val == null) {
					res = {ok: false};
					break;
				}
				val = val.replace(',', '.');
				var val = parseFloat(val);
				res = HelperTutor.validateStep.call(this.map, node, {val: val});	
			}
		break;
		case ExerciseType.INPUT:
			var val = prompt('Введите значение');
			if (val == null) {
				res = {ok: false};
				break;
			}
			val = val.replace(',', '.');
			var val = parseFloat(val);
			res = HelperTutor.validateStep.call(this.map, node, {val: val});
		break;
	}		



	if (res.ok)	{
		if (node.success != undefined) {
			var vars = {state: this.map.state, node: node, map: this.map};
			var standard = callFunctionOrEvalPro(node.success, this.map, vars);
		}

		$('div#tutor_ex_menu ul li.' + this.step + ' .tutor_step_value').text(res.standard);

		if (node.type == ExerciseType.STATE_CONTINUOUS && res.state_change != undefined) {
			this.state_change(res.state_change);
		}
		this.setNextGoal();
	}
	else {
		this.error(false);
	}
}
Tutor.prototype.getMenuItem = function() {
	s_html = '';
	for(var i = 0; i < this.ex.nodes.length; i++) {
		var node = this.ex.nodes[i];
		if (node.goal === "" && node.hint === "" )
			continue;

		s_html += "<li class=\"" + i + "\">" + (i + 1) + ". " + node.goal;
		s_html +=  "<br/><span class=\"button_hint\">" + node.hint + "</span>";
		s_html +=  '<br/><span class="tutor_step_value"></span>';
		switch(node.type)
		{
			case ExerciseType.STATE_CONTINUOUS:
			if (node.test != undefined)
				s_html +=  '<br/><a href="#" class="tutor_step_input_value">Ok</a>';
			else
				s_html +=  '<br/><a href="#" class="tutor_step_input_value">Ввести значение</a>';
			break;
			case ExerciseType.INPUT:
			s_html +=  '<br/><a href="#" class="tutor_step_input_value">Ввести значение</a>';
			break;
			case ExerciseType.STATE_DISCRETE:
			break;
		}				
		s_html += "</li>";
	}
	return s_html;
}

Tutor.prototype.makeMenuItem = function() {
	switch(this.map.mode){
		case deviceMode.TRAINING:
			var map_menu = this.map.control('menu');
			map_menu.RemoveAdditionalMenuItems();
			var s_html = this.getMenuItem();
			map_menu.AddAdditionalMenuItem("tutor_ex_menu", this.ex.name, "<ul>" + s_html + "</ul>");
			map_menu.FocusOnAdditionalMenuItem(1);
			var self = this;

			$('div#tutor_ex_menu ul li a.tutor_step_input_value').click(function() {
				self.inputValue();
			});
			break;
		case deviceMode.CONTROL:
			var map_menu = this.map.control('menu');
			map_menu.RemoveAdditionalMenuItems();
			var s_html = this.getMenuItem();
			map_menu.AddAdditionalMenuItem("tutor_ex_menu", "Контроль <span class=\"user_name\">" + this.user_firstName + " " + this.user_secondName + "</span>", this.ex.name +  "<ul>" + s_html + "</ul>");
			map_menu.FocusOnAdditionalMenuItem(1);

			var self = this;
			$('div#tutor_ex_menu ul li a.tutor_step_input_value').click(function() {
				self.inputValue();
			});
			break;
		break;
	}

}

Tutor.prototype.showCurrentGoal = function() {
	switch(this.map.mode){
		case deviceMode.TRAINING:
			$('div#tutor_ex_menu ul li').removeClass("selected");
			$('div#tutor_ex_menu ul li.' + this.step).addClass("selected");
			//$('body').scrollTo($('div#tutor_ex_menu ul li.' + this.step));
			break;
		case deviceMode.CONTROL:
			//$('span#fail').html(this.fail_count);
			console.log("error count = " + this.fail_count);
			$('div#tutor_ex_menu ul li').removeClass("selected");
			$('div#tutor_ex_menu ul li.' + this.step).addClass("selected");
			//$('body').scrollTo($('div#tutor_ex_menu ul li.' + this.step));
			break;
	}
}

Tutor.prototype.unloadExercise = function(ex) {
	delete this.ex;
	step = 0;
	this.fail_count = 0;
}

Tutor.prototype.revertMapState = function() {
	this.map.rollbackTo(this.prev_state);
	this.next_goal_state = this.prev_goal_num;
	//this.map.draw();
}


Tutor.prototype.setNextGoal = function() {
	this.last_state = this.map.stateCopy();
	this.step ++;
	if (this.step >= this.ex.nodes.length) {
		this.Finish();
		return;
	}
	this.showCurrentGoal();
}

Tutor.prototype.Finish = function() {
	alert("Задание выполнено!");
	var value = 70 + (5 - this.fail_count) * 6;
	this.ControlReport(this.user_firstName, this.user_secondName, value);
	this.ShowExListInMenu();
}

Tutor.prototype.Abort = function() {
	alert("Задание не выполнено! Допущено слишком много ошибок.");
	var value = 70 + (5 - this.fail_count) * 6;
	this.ControlReport(this.user_firstName, this.user_secondName, value);
	this.ShowExListInMenu();
}

Tutor.prototype.ControlReport = function (firstname, secondname, value) {
	var device = this.map.constructor.name;
	var ex = this.ex;
	$.getJSON(
		mainserver + "report/deviceinfo/?callback=?",
		{type : 'ins', name : secondname, soname: firstname, device: device, value: value, testname: ex.name}, 
		function() {}
	);
}

Tutor.prototype.checkEx = function() {
	return false;
}

Tutor.prototype.ShowExListInMenu = function() {
	var map_menu = this.map.control('menu');
	if (this.map.mode == deviceMode.CONTROL)
		map_menu.UnlockMenuItems();
	map_menu.loadControl();
}

Tutor.prototype.getExerciseList = function() {
	return this.param.exerciseList;
}

Tutor.prototype.getExercise = function(num) {
	return this.param.exerciseList[num];
}


HelperTutor = {}

HelperTutor.validateStep = function(node, action) {
	var vars = {state: this.state, node: node, map: this};
	var standard = callFunctionOrEvalPro(node.value, this, vars);
	var value = action.val;

	var d = Math.abs((value - standard) / standard);
	var yes = d <= this.accuracy_value;

	if (yes && (value != standard)) {
		alert('Скорректированное значение: ' + standard);
	}

	return {ok: yes, standard: standard};
}