function TutorControl(name, param) {
	TutorControl.superclass.constructor.call(this, name, param);
	this.step = 0;
}
extend(TutorControl, Logic);

TutorControl.prototype.setMap = function (map) {
	TutorControl.superclass.setMap.call(this, map);
	var self = this;
	map.addListener('state_change', this, function(control, params) {
 		if (!self.control_mode) return;
 		for(var i in self.control_mode.goals) {
			var g = self.control_mode.goals[i];
			if (g.completed(self)) {
				self.goals[i] = true;
				$('#ex_control_item' + i).css('text-decoration', 'line-through');
			}
		}

		if (self.control_mode.completed(self)) {
			self.Finish();
		}
		self.step++;
		if (self.step > self.ex.nodes.length * 3)
			self.Abort();
	});
}

TutorControl.prototype.Finish = function() {
	alert("Задание выполнено!");
	this.ShowExListInMenu();
}

TutorControl.prototype.Abort = function() {
	alert("Задание не выполнено! Допущено слишком много ошибок.");
	this.ShowExListInMenu();
}

TutorControl.prototype.ShowExListInMenu = function() {
	this.control_mode = null;
	var map_menu = this.map.control('menu');
	map_menu.UnlockMenuItems();
	map_menu.RemoveAdditionalMenuItems();
	this.start_control(map_menu);
}


TutorControl.prototype.completed_all = function() {
	for(var i in this.goals) {
		if (!this.goals[i])
			return;
	}
	return true;
}

TutorControl.prototype.getExercise = function(num) {
	return this.map.exercises[num];
}

TutorControl.prototype.getMenuItem = function() {
	html_s = '';
	for(var i in this.control_mode.goals) {
		var g = this.control_mode.goals[i];
		html_s += "<li><span href=\"#\" id=\"ex_control_item" + i + "\">" + g.title + "</span></li>"
	}
	return html_s;
}


TutorControl.prototype.start_control = function(menu) {
	this.control_mode = null;
	var tasks = this.map.exercises;
	var html_s = '';
	for(var i in tasks) {
		var t = tasks[i];
		if (!t.control_mode) continue;
		html_s += "<li><a href=\"#\" id=\"ex" + i + "\">" + t.name + "</a></li>"
	}
	menu.AddAdditionalMenuItem("exercise_list", "Задания", "<ul>" + html_s + "</ul>");
	var self = this;
	$('div#menu div:last a').each(
		function(index){ 
			var id = $(this).attr('id').substring(2);
			$(this).click(function(){
				self.loadExercise(self.getExercise(id));
			}) ;
		}
	);
}

TutorControl.prototype.makeMenuItem = function(t) {
	var map_menu = this.map.control('menu');
	map_menu.RemoveAdditionalMenuItems();
	var s_html = this.getMenuItem();
	map_menu.AddAdditionalMenuItem("tutor_ex_menu", this.ex.name, "<ul>" + s_html + "</ul>");
	map_menu.FocusOnAdditionalMenuItem(1);
	var self = this;
}

TutorControl.prototype.loadExercise = function(t) {
	this.map.control('menu').LockMenuItems();
	this.map.restart();
	this.step = 0;
	this.ex = t;
	this.goals = [];
	for(var i in t.control_mode.goals) {
		this.goals.push(false);
	}
	this.control_mode = t.control_mode;
	this.makeMenuItem();
}