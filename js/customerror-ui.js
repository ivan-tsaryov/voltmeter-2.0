//Вывод информации об ошибках

//Создание таба - Рекомендации
$(function() {
	$('#tabs > ul').append('<li><a href="#tab-e"><span>Рекомендации</span></a></li>');
	$('#tabs').append('<div id="tab-e"><ul id="errorlist"></ul></div>');
});

//Обработка вывода
//e - контроллер CustomErrorAnalysis
function capture_error(e) {
	//Вывод ошибки
	//m - сообщение
	e.func_log = function(m) {
		$('#errorlist').append('<li>' + m + '</li>');
	}
	//Очистка сообщений
	e.log_clear = function() {
		$('#errorlist').empty();	
	}
}