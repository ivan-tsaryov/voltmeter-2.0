function Device4(node, width, height) {
	Device4.superclass.constructor.call(this, node, width, height);
	this.state = {
		left: false,
		avp: false, // Включен ли АВП
		right: false,
		measurement: 1, // Режим измерения [1 - Udc, 2 -Uac, 3 - R, 4 - Idc, 5 - Iac]
		switcher: false, // Подключен прибор к питанию
		wire_I: false, // Подключен ли провод измерения токов
		wire_0: false, // Подключен ли общий провод
		wire_UR: false,// Подключен ли прибор измерения напряжения и сопротивления
		setSignal: false,
		button_push : '1'
	};
	this.start_state = jQuery.extend(true, {}, this.state);
	this.tmpState = jQuery.extend(true, {}, this.state);

	this.name = "d4";
	this.title = 'Вольтметр В7-40';
    this.signals = [20.5, 13.9, 23, -0.4981, 1.6095];

	this.state_time_variables = [];
	this.important_state = {switcher: 'Включение/выключение розетки',
							measurement: 'Режим измерения',
							wire_UR: 'Кабель К1',
							wire_0: 'Кабель К2',
							wire_I: 'Кабель К3'
	};

	this.InitEx();
} extend(Device4, Map);

// Сохранение состояния в память
Device4.prototype.saveToMemento = function () {
  return new Memento(this.state);
};

// Восстановление состояния из памяти
Device4.prototype.restoreFromMemento = function (memento) {
  this.action(this.state, memento.getSavedState(), 'change_state');
  this.state_save();
  this.state = memento.getSavedState();
};

// Координаты карты изображения
Device4.prototype.getArea = function () {
	var area = [];

    // Switcher
    area.push({shape: "rect", key: "switcher", coords: "306,284,366,368", tooltip : "Переключатель \"СЕТЬ\""});

    // Display
    area.push({shape: "rect", key: "display", coords: "398,194,785,309", tooltip: "Индикатор"});

    // Buttons
    area.push({shape: "rect", key: "button_left", coords: "839,267,882,311", tooltip : "Кнопка уменьшения предела"});
    area.push({shape: "rect", key: "button_avp", coords: "897,267,940,311", tooltip : "Кнопка АВП"});
    area.push({shape: "rect", key: "button_right", coords: "955,267,998,311", tooltip : "Кнопка увеличения предела"});
    area.push({shape: "rect", key: "button_Udc", coords: "1032,267,1075,311", tooltip : "Кнопка постоянного напряжения"});
    area.push({shape: "rect", key: "button_Uac", coords: "1090,267,1133,311", tooltip : "Кнопка переменного напряжения"});
    area.push({shape: "rect", key: "button_R", coords: "1148,267,1191,311", tooltip : "Кнопка сопротивления"});
    area.push({shape: "rect", key: "button_Idc", coords: "1205,267,1249,311", tooltip : "Кнопка постоянного тока"});
    area.push({shape: "rect", key: "button_Iac", coords: "1263,267,1307,311", tooltip : "Кнопка переменного тока"});

	// Leds
	area.push({shape: "rect", key: "led_left", coords: "844,217,875,244", tooltip : "Светодиод уменьшения предела"});
	area.push({shape: "rect", key: "led_avp", coords: "904,217,932,244", tooltip : "Светодиод АВП"});
	area.push({shape: "rect", key: "led_right", coords: "962,217,990,244", tooltip : "Светодиод увеличения предела"});
	area.push({shape: "rect", key: "led_Udc", coords: "1039,217,1067,244", tooltip : "Светодиод постоянного напряжения"});
	area.push({shape: "rect", key: "led_Uac", coords: "1097,217,1125,244", tooltip : "Светодиод переменного напряжения"});
	area.push({shape: "rect", key: "led_R", coords: "1154,217,1183,244", tooltip : "Светодиод сопротивления"});
	area.push({shape: "rect", key: "led_Idc", coords: "1213,217,1241,244", tooltip : "Светодиод постоянного тока"});
	area.push({shape: "rect", key: "led_Iac", coords: "1271,217,1298,244", tooltip : "Светодиод переменного тока"});

    //Wires
    area.push({shape: "rect", key: "wire_UR", coords:  "1349,179,1413,243", tooltip : "Провод измерения U,R"});
    area.push({shape: "rect", key: "wire_0", coords:  "1349,256,1413,320", tooltip : "Общий провод"});
    area.push({shape: "rect", key: "wire_I", coords: "1349,333,1413,397", tooltip : "Провод измерения токов"});


    // Коррекция координат в соответствии с разрешением экрана
	area.forEach(function(item) {
		var $voltmeter = $("#voltmeter");
		var height = $voltmeter.height();

		var scale = (height / 456);
		item.coords = item.coords.split(',').map(function(x, index) {
			if (index == 0 || index == 2) { // Небольшой костыль, т.к. x-позиции странно смещаются на 1 пиксель влево
				return Math.ceil(x * scale) + 1
			} else {
				return Math.ceil(x * scale)
			}
		}).join(',');
	});

	return area;
};

// Инициализация контроллеров
Device4.prototype.definitionControl = function () {
	var c = [];

	c.push({key: 'display', cls: Button, param: ""});
	c.push({key: 'switcher', cls: ButtonBoxImage, param: { cond: true, act: "d.switcher = !s.switcher, this.map.switcher()", on: "t.switcher", image_on: "img/switcher.png", image_off: null}});

	c.push({key: 'button_left', cls: Button, param: { cond: "this.map.state.switcher", act: "d.left = true,  this.map.button_left()"}});
	c.push({key: 'button_avp', cls: Button, param: { cond: "this.map.state.switcher", act: "d.avp = !s.avp, this.map.button_avp()"}});
	c.push({key: 'button_right', cls: Button, param: { cond: "this.map.state.switcher", act: "d.right = true, d.left = false, this.map.button_right()"}});
	c.push({key: 'button_Udc', cls: Button, param: { cond: "this.map.state.switcher", act: "d.measurement = 1, this.map.switch_measurement()"}});
	c.push({key: 'button_Uac', cls: Button,	param: { cond: "this.map.state.switcher", act: "d.measurement = 2, this.map.switch_measurement()"}});
	c.push({key: 'button_R', cls: Button, param: { cond: "this.map.state.switcher", act: "d.measurement = 3, this.map.switch_measurement()"}});
	c.push({key: 'button_Idc',  cls: Button, param: { cond: "this.map.state.switcher", act: "d.measurement = 4, this.map.switch_measurement()"}});
	c.push({key: 'button_Iac', cls: Button, param: { cond: "this.map.state.switcher", act: "d.measurement = 5, this.map.switch_measurement()"}});

	c.push({key: 'wire_I', cls: ButtonBoxImage, param: { cond: "this.map.state.switcher", act: "d.wire_I = !s.wire_I, this.map.plug_wire()", on: "t.switcher && t.wire_I", image_on: "img/wire_I.png", image_off: null}});
	c.push({key: 'wire_0', cls: ButtonBoxImage, param: { cond: "this.map.state.switcher", act: "d.wire_0 = !s.wire_0, this.map.plug_wire()", on: "t.switcher && t.wire_0", image_on: "img/wire_0.png", image_off: null}});
	c.push({key: 'wire_UR', cls: ButtonBoxImage, param: { cond: "this.map.state.switcher", act: "d.wire_UR = !s.wire_UR, this.map.plug_wire()", on: "t.switcher && t.wire_UR", image_on: "img/wire_UR.png", image_off: null}});

	c.push({key: 'led_left', cls: ButtonBoxImage, param: { on: "t.switcher && t.left"}});
	c.push({key: 'led_avp', cls: ButtonBoxImage, param: { on: "t.switcher && t.avp", image_on: "img/led_avp.png", image_off: null}});
	c.push({key: 'led_right', cls: ButtonBoxImage, param: { on: "t.switcher && t.right"}});
	c.push({key: 'led_Udc', cls: ButtonBoxImage, param: { on: "t.switcher && t.measurement == 1", image_on: "img/led_Udc.png", image_off: null}});
	c.push({key: 'led_Uac', cls: ButtonBoxImage, param: { on: "t.switcher && t.measurement == 2", image_on: "img/led_Uac.png", image_off: null}});
	c.push({key: 'led_R', cls: ButtonBoxImage, param: { on: "t.switcher && t.measurement == 3", image_on: "img/led_R.png", image_off: null}});
	c.push({key: 'led_Idc', cls: ButtonBoxImage, param: { on: "t.switcher && t.measurement == 4", image_on: "img/led_Idc.png", image_off: null}});
	c.push({key: 'led_Iac', cls: ButtonBoxImage, param: { on: "t.switcher && t.measurement == 5", image_on: "img/led_Iac.png", image_off: null}});


	c.push({key: 'generator', cls: Device4Helper.Generator});
	c.push({key: 'tutor', cls: Tutor, param: null});
	c.push({key: 'menu', cls: Menu, param: null});

	c.push({key: 'customerror', cls: CustomErrorAnalysis, param: null});

	return c;
};

Device4.prototype.resetMenu = function () {
	for (var i = 0; i < this.controls.length; i++) {
		if (this.controls[i].name == "menu_bar") {
			this.menu_bar = this.controls[i];
		}
		if (this.controls[i].name == "generator") {
			this.generator = this.controls[i];
		}
	}
};

Device4Helper = {};


// Функции контроллеров
Device4.prototype.switcher = function () {
	if (this.state.switcher) {
		this.generator.start();
	} else {
		this.generator.stop();

		this.generator.limit = 4;
		this.generator.unit = 2;
	}
};

Device4.prototype.button_left = function() {
	if (!this.state.avp && !this.generator.overflow) {
		var limit = this.generator.limit;

		if (limit > 1) {
			this.generator.limit = limit - 1;
		} else {
			if (this.generator.unit > 1) {
				this.generator.limit = 5;
				this.generator.unit = this.generator.unit - 1;
			}
		}
		this.generator.stop();
		this.generator.start();
	}
};

Device4.prototype.button_avp = function(){
	this.generator.stop();
	this.generator.start();
};

Device4.prototype.button_right = function(){
	if (!this.state.avp) {
		if (this.generator.limit < 5) {
			this.generator.limit = this.generator.limit + 1;
		} else {
			if (this.generator.unit == 2 && this.state.measurement != 3) {
				return;
			}

			if (this.generator.unit < 3) {
				this.generator.unit = this.generator.unit + 1;
				this.generator.limit = 1;

				this.generator.number = this.generator.number / 1000;

				if (Math.floor(this.generator.number) == 1 || this.generator.number == 0) {
					this.generator.limit = 1;
				} else {
					this.generator.limit = 2;
				}
			}
		}
		this.generator.stop();
		this.generator.start();
	}
};

Device4.prototype.switch_measurement = function() {
	this.generator.stop();
	this.generator.start();
};

Device4.prototype.plug_wire = function () {
	this.generator.stop();
	this.generator.start();
};


Device4.prototype.no_values = function (array) {
	for (var i = 0; i < array.length; i++)
		if (array[i] != "") return false;
	return true;
};

// Установка собственного сигнала из таблицы во вкладке "Входные сигналы"
setOwnSignal = function() {
    var $tr = $(this).parent().find("input:checked").closest("tr");
    var classname = $tr.prop("class");
    if (classname == "fields") {
        var $input_field = $tr.find("input");
        device.signals[0] = $input_field.eq(1).val();
        device.signals[1] = $input_field.eq(2).val();
        device.signals[2] = $input_field.eq(3).val();
        device.signals[3] = $input_field.eq(4).val();
        device.signals[4] = $input_field.eq(5).val();
    } else {
        var $input_td = $tr.find("td");
        device.signals[0] = $input_td.eq(1).text();
        device.signals[1] = $input_td.eq(2).text();
        device.signals[2] = $input_td.eq(3).text();
        device.signals[3] = $input_td.eq(4).text();
        device.signals[4] = $input_td.eq(5).text();
    }
    if (device.no_values(device.signals)) {
        $(this).qtip({
            show: { ready: true },
            content: { text: 'Задайте хотя бы один параметр!' },
            hide: { fixed: true, delay: 2000 },
            position: { my: 'left center', at: 'right center' },
            style: { classes: "qtip-bootstrap", tip: { corner: "left center" } }
        });

        setTimeout(function() {
            $('.qtip').qtip('destroy', true);
        }, 2000);
    } else {
		device.state.button_push = "radio";
		device.state.setSignal = true;
		var control = new Control;
		device.actionManager.click(control);
        $(this).qtip({
            show: { ready: true },
            content: { text: 'Значения заданы' },
            hide: { fixed: true, delay: 2000 },
            position: { my: 'left center', at: 'right center' },
            style: { classes: "qtip-bootstrap", tip: { corner: "left center" } }
        });

        setTimeout(function() {
            $('.qtip').qtip('destroy', true);
        }, 2000);
    }
};

// Загрузка документации
Device4.prototype.getHelpText = function() {
	var docs = ["docs/Назначение.htm","docs/Комплект%20поставки.htm","docs/Технические%20данные.htm","docs/Общие%20указания%20по%20эксплуатации.htm","docs/Меры%20предосторожности.htm","docs/Подготовка%20к%20работе.htm","docs/Порядок%20работы%20.htm"];
	var thesises = "";
	
	docs.forEach(function(item) {
		thesises += device.getDocsFromFile(item);
	});
	return thesises;
};

// Получение содержимого файла по ссылке
Device4.prototype.getDocsFromFile = function(url) {
	var filePath = url;
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.open("GET",filePath,false);
	xmlhttp.send(null);
	
	return xmlhttp.responseText;
};

// Для экспертной системы
Device4.prototype.functionalBlockChangeState = function (state1, state2) {

	var delta = state2.diff(state1);
	var r = {};
	for(var key in this.important_state) {
		if (delta.get(key) == undefined) continue;
		r[key] = 'goal';
	}
	return r;
};

// Для экспертной системы
Device4.prototype.functionalBlock = function() {
	var g = this;
	return {
		changeState: function(state1, state2) {
			return g.functionalBlockChangeState(state1, state2)
		}, 
		title: function(block) {return g.important_state[block]}
	}
};