Device4Helper.Generator = function(name, param) {
	Device4Helper.Generator.superclass.constructor.call(this, name, param);
	this.timer = null;
	this.sign = true;
	this.number = null;
	this.limit = 4;
	this.unit = 2;
	this.overflow = null;
};
extend(Device4Helper.Generator, Logic);

Device4Helper.Generator.prototype.setMap = function (map) {
	Device4Helper.Generator.superclass.setMap.call(this, map);
};

Device4Helper.Generator.prototype.findNumber = function (measurement) {
	if (measurement == 1) {
		return parseFloat(device.signals[0]);
	} else if (measurement == 2) {
		return parseFloat(device.signals[1]);
	} else if (measurement == 3) {
		return parseFloat(device.signals[2] / 1000);
	} else if (measurement == 4) {
		return parseFloat(device.signals[3]) * 1000;
	} else if (measurement == 5) {
		return parseFloat(device.signals[4]) * 1000;
	}
};

Device4Helper.Generator.prototype.setNumber = function (measurement) {
	var number = this.findNumber(measurement);

	if (number >= 0) {
		this.sign = true;
	} else {
		this.sign = false;
	}
	if (this.unit == 1) {
		number = number*1000;
	} else if (this.unit == 3) {
		if (device.state.measurement == 3) {
			number = number / 1000;
		} else {
			this.unit = 2;
			this.limit = 5;
		}
	}
	this.number = Math.abs(number);
};

// Проверяет, готов ли прибор к отображению числа на дисплее (активны ли нужные контроллеры)
Device4Helper.Generator.prototype.isReady = function () {
	if ([1, 2, 3].indexOf(device.state.measurement) != -1) {
		if (device.state.wire_UR && device.state.wire_0) {
			this.setNumber(device.state.measurement);
			return true;
		} else {
			return false;
		}
	} else {
		if (device.state.wire_I && device.state.wire_0) {
			this.setNumber(device.state.measurement);
			return true;
		} else {
			return false;
		}
	}
};

// Выключает дисплей
Device4Helper.Generator.prototype.turnOffDisplay = function() {
	$("[id*=num]").filter(":visible").invisible();
	$("[id*=limit]").filter(":visible").invisible();
	$("[id*=sign]").filter(":visible").invisible();
	$("[id*=unit]").filter(":visible").invisible();

	this.sign = true;
};

// Сбрасывает дисплей на стандартные значения
Device4Helper.Generator.prototype.resetDisplay = function() {
	$("[id*=num]").filter(":visible").invisible();
	$("[id*=limit]").filter(":visible").invisible();
	$("[id*=sign]").filter(":visible").invisible();
	$("[id*=unit]").filter(":visible").invisible();

	$("#limit_"+this.limit).visible();

	if (this.sign && device.state.measurement != 3) {
		$("#sign_plus").visible();
	} else if (device.state.measurement != 3) {
		$("#sign_minus").visible();
	}

	$("#unit_"+this.unit).visible();

	for (var i = 1; i <= 5; i++) {
		$("#num_"+i+"_0").visible();
	}
};

// Показывает переполнение на дисплее
Device4Helper.Generator.prototype.showOverflow = function() {
	clearInterval(this.timer);
	this.timer = setInterval(function() {
		$("#num_2_0").imgToggle();
		$("#num_3_0").imgToggle();
		$("#num_4_0").imgToggle();
		$("#num_5_0").imgToggle();
	}, 100);
};

// Проверяет, сможет ли отобразиться данное число на данном пределе
Device4Helper.Generator.prototype.isShowable = function(number, limit) {
	number = Math.abs(number);
	return !(number > 0.19999 * Math.pow(10, limit) || (number < 1 && limit == 1));
};

// Автоматический выбор предела
Device4Helper.Generator.prototype.autoLimitSelect = function() {
	var number = this.number;

	if (this.unit == 1) {
		number = number/1000;
	} else if (this.unit == 3) {
		number = number*1000;
	}

	for (var i = 1; i <= 5; i++) {
		if (this.isShowable(number*1000, i)) {
			this.limit = i;
			this.unit = 1;
			this.setNumber(device.state.measurement);

			return;
		}
	}

	for (var i = 1; i <= 5; i++) {
		if (this.isShowable(number, i)) {
			this.limit = i;
			this.unit = 2;
			this.setNumber(device.state.measurement);
			return;
		}
	}
	if (device.state.measurement == 3) {
		for (var i = 1; i <= 5; i++) {
			if (this.isShowable(number/1000, i)) {
				this.limit = i;
				this.unit = 3;
				this.setNumber(device.state.measurement);
				return;
			}
		}
	}
};

// Отображает нужные изображения с соответствующими параметрами
Device4Helper.Generator.prototype.indicateNumber = function(number) {
	this.resetDisplay();
	$("#unit_"+this.unit).visible();
	$("#limit_"+this.limit).visible();

	if (!isNaN(number)) {
		number = number.toString();

		number = (number[0] == "-") ? number.replace("-", "") : number;

		var integer = number.split(/[,.]/)[0];
		var fractional = number.split(/[,.]/)[1];

		if (fractional === undefined)
			fractional = "";

		var index = this.limit - integer.length + 1;

		for (var i = 0; i < integer.length; i++) {
			$("[id*=num_" + index + "]").filter(":visible").invisible();
			$("#num_" + index + "_" + integer[i]).visible();
			index++;
		}
		index = this.limit + 1;
		for (var j = 0; j < 5 - this.limit; j++) {
			$("[id*=num_" + index + "]").filter(":visible").invisible();

			if (fractional[j] === undefined || fractional === undefined) {
				$("#num_" + index + "_" + 0).visible();
			} else {
				$("#num_" + index + "_" + fractional[j]).visible();
			}
			index++;
		}
	}
};

// Подготовка к отображению на дисплее
Device4Helper.Generator.prototype.showNumber = function() {
	if (device.state.avp) {
		this.autoLimitSelect();
	}
	var number = this.number;

	if (this.sign && device.state.measurement != 3) {
		$("#sign_plus").visible();
	} else if (device.state.measurement != 3) {
		$("#sign_minus").visible();
	}
	var limit = this.limit;

	if (number > 0.19999*Math.pow(10,limit) || (number <= 1 && limit == 1 && number != 0)) {
		if (this.unit > 1) {
			if (this.isShowable(number*1000, 5)) {
				this.unit = this.unit - 1;

				this.limit = 5;

				this.number = this.number*1000;
				this.overflow = false;
				clearInterval(this.timer);
				this.indicateNumber(this.number);
			} else {
				this.resetDisplay();
				this.overflow = true;
				this.showOverflow();
			}
		} else {
			this.resetDisplay();
			this.overflow = true;
			this.showOverflow();
		}
	} else {
		this.overflow = false;
		clearInterval(this.timer);
		this.indicateNumber(number);
	}
};

// Включает показ чисел на дисплее
Device4Helper.Generator.prototype.start = function() {
	$("#switcher").visible();
	$("#led_Udc").visible();

	if (this.isReady()) {
		this.showNumber();
	} else {
		this.resetDisplay();
	}
};

// Выключает дисплей
Device4Helper.Generator.prototype.stop = function() {
	$(".controllers, .indicators").filter(":visible").invisible();

	clearInterval(this.timer);
	this.turnOffDisplay();
};