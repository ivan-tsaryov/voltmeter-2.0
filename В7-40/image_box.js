function ImageBox(name, param) {
	ImageBox.superclass.constructor.call(this, name, param);
	this.param = param;
}
extend(ImageBox, Control);

ImageBox.prototype.setRender = function(render) {
	ImageBox.superclass.setRender.call(this, render);
}

ImageBox.prototype.setMap = function (map) {
	ImageBox.superclass.setMap.call(this, map);
	
	var b = this.box();
	
	if (this.param.image != null)
	{
		var a = this.param.image.split(';');
		this.param.image = {src: a[0], w: a[1], h: a[2]};
	}
	
	var area = {shape: "image", coords: b.x + "," + b.y + "," + b.width + "," + b.height};
	area.tooltip = this.render.attr.tooltip;
	area.hint_text = this.render.attr.hint_text;
	if (this.param.top) {
		area.coords = (b.x + b.width/2)  + "," + b.y + ",5";
	}
	//this.render.remove();
	this.render = new Render(area, this);
	//this.render.getNode(map);
	map.addListener('state_change', this, function() {
    this.render.setImage(this.param.image.src, this.param.image.w, this.param.image.h);
    this.render.opacity(1);
	})
}
