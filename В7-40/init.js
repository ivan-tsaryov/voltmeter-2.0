function setMode(map, mode) {
	map.setMode(mode);
	if (map.menu_bar) {
		device.resetMenu();
	}
	map.draw();
	switch (mode) {
		case deviceMode.EMULATOR:
			map.start();
			$("h3#mode_header").text("Режим эмулятора");
			var bar = map.GetControlByName('tutor');
			bar.unloadExercise();
			//map.RemoveAdditionalMenuItems();
			break;
		case deviceMode.HELP:
			$("text").css("display", "none");
			$('div#placeholder').css("display", "none");
			$("h3#mode_header").text("Режим справки");
			var bar = map.GetControlByName('tutor');
			bar.unloadExercise();
			//map.RemoveAdditionalMenuItems();
			break;
		case deviceMode.TRAINING:
			//device.turnOff();
			//map.generator.stop();
		case deviceMode.CONTROL:
			if (map.menu_bar) {
				map.menu_bar.menuRectangeRedraw();
			}
			//device.turnOff();
			//map.generator.stop();
			$("h3#mode_header").text("Режим тренажера");
			var bar = map.GetControlByName('tutor');
			bar.unloadExercise();
			var foo = map.GetExList();
			var qux = map;
			//map.RemoveAdditionalMenuItems();
			/*var html_s = "";
			for (var i = 0; i < foo.length; i++) {
				html_s += "<li><a href=\"#\" id=\"ex" + i + "\">" + foo[i] + "</a></li>"
			}
			map.AddAdditionalMenuItem("exercise_list", "Задания", "<ul>" + html_s + "</ul>");
			$('div#menu div:last a').each(function (index) { $(this).click(function () { bar.loadExercise(qux.GetEx(index)) }) });
			map.FocusOnAdditionalMenuItem(1);*/
			break;
			//case deviceMode.CONTROL:
			//map.restart();
			//break;
	}
}

$(document).ready(function () {
	$('#tabs').tabs({ heightStyle: "auto" });

	$("body").find("button").button();

	var $img = $("#device");

	var menu = {};
	device = new Device4($('#map'), $img.width(), $img.height());
	device.renderAreaControl();
	device.resetMenu();
	device.draw();
	
	menu = device.GetControlByName('menu');
	capture_error(device.control('customerror'));
	
	String.prototype.format = function() {
		var args = arguments;
		args['{'] = '{';
		args['}'] = '}';
		return this.replace(
			/{({|}|-?[0-9]+)}/g,
			function(item) {
				var result = args[item.substring(1, item.length - 1)];
				return typeof result == 'undefined' ? '' : result;
				}
			);
	};

	// Фильтр вводимых значений в поля ввода значений (разрешаются только числа и точка)
	$('.fields').find("input").keyup(function(){
		var val = $(this).val();
		if(isNaN(val)){
			val = val.replace(/[^0-9\.-]/g,'');
			if(val.split('.').length > 2)
				val = val.replace(/\.+$/,"");
		}
		$(this).val(val);
	});

	$('#setValues').on("click", setOwnSignal);
});

$(window).load(function() {
	// Коррекция координат контроллеров прибора в соответствии с разрешением экрана (разрешением уменьшенного изображения)
	/*$('svg').find('rect').each(function() {
		var $img = $("#device");
		var height = $img.height();

		var scale = height / 456;


		$(this).attr('x', Math.round($(this).attr('x')*scale));
		$(this).attr('y', Math.round($(this).attr('y')*scale));
		$(this).attr('width', Math.round($(this).attr('width')*scale));
		$(this).attr('height', Math.round($(this).attr('height')*scale));
	});

	// Загрузка документации в блок документации"
	$("#part1").load("docs/Назначение.htm");
	$("#part2").load("docs/Комплект%20поставки.htm");
	$("#part3").load("docs/Технические%20данные.htm");
	$("#part4").load("docs/Общие%20указания%20по%20эксплуатации.htm");
	$("#part5").load("docs/Меры%20предосторожности.htm");
	$("#part6").load("docs/Подготовка%20к%20работе.htm");
	$("#part7").load("docs/Порядок%20работы%20.htm");
	*/
});