function BaseMenuBehaviour(menu) {
  this.menu = menu;
  this.number_callbacks = {6 : function() {this.menu.map.setInput("inputA");this.menu.map.setMenu("menu_input");this.menu.map.resetMenu(true);},
                           7 : function() {this.menu.map.setInput("inputB");this.menu.map.setMenu("menu_input");this.menu.map.resetMenu(true);},
                           8 : function() {this.menu.map.setMenu("menu_settings");this.menu.map.resetMenu(true);},
                           9 : function() {this.menu.map.setMenu("menu_math_limit");this.menu.map.resetMenu(true);}
  }
}

BaseMenuBehaviour.prototype.selectItem = function () {
  menu = this.menu;
  var selected_menu = menu.menu_elements_configuration[menu.current_pos_way][menu.current_index].submenu;
  var previous_position = menu.current_index;
  cnt90.generator.selectedInput();
  if(selected_menu != undefined) {
    prev_menu = menu.menu_elements_configuration[menu.current_pos_way][menu.current_index].key;
    menu.menu_elements_configuration[menu.current_pos_way][menu.current_index].previous_menu = prev_menu;
    for(var i = 0; i < menu.menu_elements_configuration[selected_menu].length; i++) {
      this.menu.menu_elements_configuration[selected_menu][i]['previous_menu_position'] = previous_position;
    }
    menu.map.setMenu(selected_menu);
    menu.map.resetMenu();
  } else {
    this.menu.map.state.measurement = menu.menu_elements_configuration[menu.current_pos_way][menu.current_index].key;
    this.menu.map.state.current_pos_way = menu.findPreviousMenu();
  }

}

BaseMenuBehaviour.prototype.number_input = function(number) {
  if(number > 5 && number < 10) {
    this.number_callbacks[number].call(this);
  }
}

BaseMenuBehaviour.prototype.getDown = function () {

}

BaseMenuBehaviour.prototype.getUp = function () {

}

BaseMenuBehaviour.prototype.sign_button_press = function() {
}

BaseMenuBehaviour.prototype.first_empty_button_press = function() {
}

BaseMenuBehaviour.prototype.second_empty_button_press = function() {
}

BaseMenuBehaviour.prototype.getNext = function () {
  menu = this.menu;
  var new_index = menu.current_index;
  new_index++;
  if(new_index >= menu.menu_elements.length) {
    new_index = menu.menu_elements.length - 1;
  }
  menu.resetActiveElement(new_index);
  menu.draw();

}

BaseMenuBehaviour.prototype.getPrev = function () {
  menu = this.menu;
  var new_index = menu.current_index;
  new_index--;
  if(new_index < 0) {
    new_index = 0;
  }
  menu.resetActiveElement(new_index);
  menu.draw();
}
