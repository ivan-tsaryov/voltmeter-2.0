function CustomNumberMenuBehaviour(menu) {
  customNumberMenuBehaviour.superclass.constructor.call(this, menu);
}

extend(CustomNumberMenuBehaviour, BaseMenuBehaviour);

CustomNumberMenuBehaviour.prototype.getDown = function () {
}

CustomNumberMenuBehaviour.prototype.getUp = function () {
}

CustomNumberMenuBehaviour.prototype.getPrev = function () {
}

CustomNumberMenuBehaviour.prototype.getNext = function () {
}

CustomNumberMenuBehaviour.prototype.number_input = function (number) {
  this.menu.map.state.text = number;
}
