function MantissaMenuBehaviour(menu) {
  MantissaMenuBehaviour.superclass.constructor.call(this, menu);
  menu.current_index = 1;
  this.active_control = menu.menu_elements[menu.current_index];
  this.active_control.activate();
  this.menu_and_limits_matching = {'count_mantis': 'Mantissa', 'count_exp': 'Count'}
}

extend(MantissaMenuBehaviour, BaseMenuBehaviour);


MantissaMenuBehaviour.prototype.number_input = function(number) {
  state_limit_property = this.findStateLimit();
  value = this.menu.map.state.limits[state_limit_property];
  this.menu.map.state.limits[state_limit_property] = value / Math.abs(value) * number;
  this.saveToState();
}

MantissaMenuBehaviour.prototype.mantissaTransfer = function (mant, order){
  return mant * Math.pow(10, order);
}

MantissaMenuBehaviour.prototype.findStateLimit = function() {
    index_of_limit = this.menu.menu_elements_configuration[this.menu.current_pos_way][this.menu.current_index]['previous_menu_position'];  
  //type_of_limit = this.menu.menu_elements_configuration[prev_menu][index_of_limit].key;
  //type_of_limit = type_of_limit.substring(0, type_of_limit.length - 4) + "erLimit";
  current_key = this.menu.menu_elements_configuration[this.menu.current_pos_way][this.menu.current_index].key;
  return type_of_limit + this.menu_and_limits_matching[current_key];
}


MantissaMenuBehaviour.prototype.saveToState = function () {
  state_limit_property = this.findStateLimit();
  type_of_limit = state_limit_property.substring(0, 10);
  count = this.menu.map.state.limits[type_of_limit + "Count"]
  mantissa = this.menu.map.state.limits[type_of_limit + "Mantissa"]
  this.menu.map.state.limits[type_of_limit] = this.mantissaTransfer(mantissa, count);
}

MantissaMenuBehaviour.prototype.getDown = function () {
  state_limit_property = this.findStateLimit();
  value = this.menu.map.state.limits[state_limit_property];
  value++;
  this.menu.map.state.limits[state_limit_property] = value;
  this.saveToState();
}

MantissaMenuBehaviour.prototype.getUp = function () {
  state_limit_property = this.findStateLimit();
  value = this.menu.map.state.limits[state_limit_property];
  value++;
  this.menu.map.state.limits[state_limit_property] = value;
  this.saveToState();
}

BaseMenuBehaviour.prototype.sign_button_press = function() {
  state_limit_property = this.findStateLimit();
  value = this.menu.map.state.limits[state_limit_property];
  value *= -1;
  this.menu.map.state.limits[state_limit_property] = value;
  this.saveToState();
}

MantissaMenuBehaviour.prototype.getPrev = function () {
}

MantissaMenuBehaviour.prototype.getNext = function () {
}

MantissaMenuBehaviour.prototype.first_empty_button_press = function () {
  var menu = this.menu;
  menu.current_index = 1;
  this.active_control.deactivate();
  this.active_control = menu.menu_elements[menu.current_index];
  this.active_control.activate();
}


MantissaMenuBehaviour.prototype.second_empty_button_press = function () {
  var menu = this.menu;
  menu.current_index = 3;
  this.active_control.deactivate();
  this.active_control = menu.menu_elements[menu.current_index];
  this.active_control.activate();
}
