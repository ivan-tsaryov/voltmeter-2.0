function MenuAreaBuilder() {
  	MenuAreaBuilder.superclass.constructor.call(this);
	
  this.map = null;
}

extend(MenuAreaBuilder, Logic);

MenuAreaBuilder.prototype.setMap = function (map) {
  this.map = map;
}

MenuAreaBuilder.prototype.set_fonts = function (area) {
	for (var i = 0; i < area.length; i++) {
		area[i] = jQuery.extend(true, area[i], {font_size: 12, font_family: "Tahoma, Verdana, serif", font_weight: "100"});
	}
}
//Meas func
MenuAreaBuilder.prototype.menu_meas_func = function () {
	menu_area = [];
	menu_area.push({type: 'menu', shape: "rect", key: "Freq", coords: "100,341,230,368", tooltip: "", hint_text: "", font_size: 16, depth: 0, font_family: "Arial, Helvetica, sans-serif"});
	menu_area.push({type: 'menu', shape: "rect", key: "Time", coords: "234,341,372,368", tooltip: "", hint_text: "", font_size: 16, depth: 0, font_family: "Arial, Helvetica, sans-serif"});
	menu_area.push({type: 'menu', shape: "rect", key: "Phase", coords: "372,341,510,368", tooltip: "", hint_text: "", font_size: 16, depth: 0, font_family: "Arial, Helvetica, sans-serif"});
	menu_area.push({type: 'menu', shape: "rect",  key: "Volt", coords: "510,341,648,368", tooltip: "", hint_text: "", font_size: 16, depth: 0, font_family: "Arial, Helvetica, sans-serif"});
	this.set_fonts(menu_area);
	return menu_area;
}
// Phase

MenuAreaBuilder.prototype.menu_meas_func_phase = function () {
	menu_area = [];
	menu_area.push({type: 'menu', shape: "rect", key: "A_and_B", coords: "118,336,248,365", hint_text: "Phase A & B"});
	menu_area.push({type: 'menu', shape: "rect", key: "A_and_C", coords: "285,336,424,364", hint_text: "Phase A & C"});
	menu_area.push({type: 'menu', shape: "rect", key: "B_and_C", coords: "464,337,608,364", hint_text: "Phase B & C"});
	this.set_fonts(menu_area);
	return menu_area;
}

//Frequentcy
MenuAreaBuilder.prototype.menu_meas_func_freq = function () {
	menu_area = [];
	menu_area.push({type: 'menu', shape: "rect", key: "meas_func_freq", coords: "118,336,248,365", hint_text: "Freq"});
	menu_area.push({type: 'menu', shape: "rect", key: "meas_func_freq_rat", coords: "285,336,424,364", hint_text: "Freq Ratio"});
	menu_area.push({type: 'menu', shape: "rect", key: "meas_func_brust", coords: "464,337,608,364", hint_text: "Brust"});
	//this['MenuAreaBuilder'].prototype['foo'].call();
	this.set_fonts(menu_area);
	return menu_area;
}

MenuAreaBuilder.prototype.menu_meas_func_freq_freq = function () {
	menu_area = [];
	menu_area.push({type: 'menu', shape: "rect", key: "meas_func_freq_input_a", coords: "118,336,248,365", hint_text: "Input A"});
	menu_area.push({type: 'menu', shape: "rect", key: "meas_func_freq_input_b", coords: "285,336,424,364", hint_text: "Input B"});
	menu_area.push({type: 'menu', shape: "rect", key: "meas_func_freq_input_c", coords: "464,337,608,364", hint_text: "Input C"});
	this.set_fonts(menu_area);
	return menu_area;
}

MenuAreaBuilder.prototype.menu_freq_ratio = function() {
	var area = [];
	area.push({type: 'menu', shape: "rect", key: "A/B", coords: "114,344,240,362", hint_text: "A/B"});
	area.push({type: 'menu', shape: "rect", key: "B/A", coords: "243,344,361,362", hint_text: "B/A"});
	area.push({type: 'menu', shape: "rect", key: "C/A", coords: "364,344,491,362", hint_text: "C/A"});
	area.push({type: 'menu', shape: "rect", key: "C/B", coords: "495,344,635,362", hint_text: "C/B"});
	this.set_fonts(area);
	return area;
}
//Volt
MenuAreaBuilder.prototype.menu_meas_func_volt = function () {
	menu_area = [];
	menu_area.push({type: 'menu', shape: "rect", key: "meas_func_volt_input_a", coords: "118,336,248,365", hint_text: "Input A"});
	menu_area.push({type: 'menu', shape: "rect", key: "meas_func_volt_input_b", coords: "285,336,424,364", hint_text: "Input B"});
	menu_area.push({type: 'menu', shape: "rect", key: "meas_func_volt_input_c", coords: "464,337,608,364", hint_text: "Input C"});
	this.set_fonts(menu_area);
	return menu_area;
}

//Time
MenuAreaBuilder.prototype.menu_meas_func_time = function () {
	menu_area = [];
	menu_area.push({type: 'menu', shape: "rect", key: "meas_func_time_interval", coords: "114,344,240,362", hint_text: "Time interval"});
	menu_area.push({type: 'menu', shape: "rect", key: "meas_func_rise_fall_time", coords: "243,344,361,362", hint_text: "Rise/Fall time"});
	menu_area.push({type: 'menu', shape: "rect", key: "meas_func_pulse_width", coords: "364,344,491,362", hint_text: "Pulse width"});	
	menu_area.push({type: 'menu', shape: "rect", key: "meas_func_duty", coords: "495,344,635,362", hint_text: "Duty"});		
	this.set_fonts(menu_area);
	return menu_area;
}

//Time Interval

MenuAreaBuilder.prototype.time_interval = function () {
	menu_area = [];
	menu_area.push({type: 'menu', shape: "rect", key: "IntervalAtoB", coords: "118,336,248,365", hint_text: "Interval A to B"});
	menu_area.push({type: 'menu', shape: "rect", key: "IntervalBtoA", coords: "285,336,424,364", hint_text: "Interval B to A"});
	this.set_fonts(menu_area);
	return menu_area;
}

//rise fall time

MenuAreaBuilder.prototype.rise_fall_time = function() {
	var area = [];
	area.push({type: 'menu', shape: "rect", key: "rise_input_a", coords: "114,344,240,362", hint_text: "Rise input A"});
	area.push({type: 'menu', shape: "rect", key: "fall_input_a", coords: "243,344,361,362", hint_text: "Fall input A"});
	area.push({type: 'menu', shape: "rect", key: "rise_input_b", coords: "364,344,491,362", hint_text: "Rise input B"});
	area.push({type: 'menu', shape: "rect", key: "fall_input_b", coords: "495,344,635,362", hint_text: "Fall input B"});
	this.set_fonts(area);
	return area;
}

// pulse width

MenuAreaBuilder.prototype.pulse_width = function () {
	menu_area = [];
	menu_area.push({type: 'menu', shape: "rect", key: "pulse_width_a", coords: "118,336,248,365", hint_text: "Pulse width A"});
	menu_area.push({type: 'menu', shape: "rect", key: "pulse_width_b", coords: "285,336,424,364", hint_text: "Pulse width B"});
	this.set_fonts(menu_area);
	return menu_area;
}

// Duty

MenuAreaBuilder.prototype.duty = function () {
	menu_area = [];
	menu_area.push({type: 'menu', shape: "rect", key: "duty_a", coords: "118,336,248,365", hint_text: "Duty A"});
	menu_area.push({type: 'menu', shape: "rect", key: "duty_b", coords: "285,336,424,364", hint_text: "Duty B"});
	this.set_fonts(menu_area);
	return menu_area;
}

MenuAreaBuilder.prototype.menu_statplot_numerical = function() {
	var area = [];
	area.push({shape: "rect", key: "stat_plot_mean_label", coords: "135,294,165,310", hint_text: "N:" , tooltip: "Mean"});
	area.push({shape: "rect", key: "stat_plot_mean", coords: "167,294,370,310", hint_text: "1536 of 2000000000000 (0%)", tooltip: "Mean"});
	area.push({shape: "rect", key: "stat_plot_max_label", coords: "127,316,166,332", hint_text: "Max:" , tooltip: "Max"});
	area.push({shape: "rect", key: "stat_plot_max", coords: "167,316,313,332", hint_text: "10.000 041 96 MHz"});
	area.push({shape: "rect", key: "stat_plot_min_label", coords: "127,338,167,353", hint_text: "Min:" , tooltip: "Min"});
	area.push({shape: "rect", key: "stat_plot_min", coords: "168,338,313,353", hint_text: "10.000 041 17 MHz"});
	area.push({shape: "rect", key: "stat_plot_p_p_label", coords: "419,295,455,311", hint_text: "P-P:" , tooltip: "p-p"});
	area.push({shape: "rect", key: "stat_plot_p_p", coords: "457,295,594,311", hint_text: "0.79 Hz"});
	area.push({shape: "rect", key: "stat_plot_adev_label", coords: "418,317,406,333", hint_text: "Adev:" , tooltip: "adev"});
	area.push({shape: "rect", key: "stat_plot_adev", coords: "458,317,554,332", hint_text: "0.12 Hz"});
	area.push({shape: "rect", key: "stat_plot_std_label", coords: "418,338,408,352", hint_text: "Std:" , tooltip: "stq"});
	area.push({shape: "rect", key: "stat_plot_std", coords: "459,338,555,352", hint_text: "0.12 Hz"});
	this.set_fonts(area);
	return area;
}

MenuAreaBuilder.prototype.menu_statplot_trendplot = function() {
	var area = [];
	area.push({type: "menu", shape: "rect", key: "trend", coords: "115,248,641,336", hint_text: "trend"});
	area.push({type: "menu", shape: "rect", key: "upper_lim", coords: "115,229,326,246", hint_text: "upper_lim"});
	area.push({type: "menu", shape: "rect", key: "lower_lim", coords: "526,231,595,246", hint_text: "lower_lim"});
	area.push({type: "menu", shape: "rect", key: "measure", coords: "116,341,325,357", hint_text: "measure"});
	area.push({type: "menu", shape: "rect", key: "time", coords: "542,343,630,358", hint_text: "time"});
	this.set_fonts(area);
	return area;
}

MenuAreaBuilder.prototype.menu_settings = function () {
	var area = [];
	area.push({type: "menu", shape: "rect", key: "meas_time", coords: "109,336,194,364", hint_text: "MeasTime 10ms", font_size: 10, depth: 0, font_family: "Arial, Helvetica, sans-serif"});
	area.push({type: "menu", shape: "rect", key: "burst", coords: "194,336,249,364", hint_text: "Burst", font_size: 10, depth: 0, font_family: "Arial, Helvetica, sans-serif"});
	area.push({type: "menu", shape: "rect", key: "arm", coords: "250,336,295,364", hint_text: "Arm", font_size: 10, depth: 0, font_family: "Arial, Helvetica, sans-serif"});
	area.push({type: "menu", shape: "rect", key: "trigger_hold_off", coords: "297,336,417,363", hint_text: "Trigger Hold Off (Off)", font_size: 10, depth: 0, font_family: "Arial, Helvetica, sans-serif"});
	area.push({type: "menu", shape: "rect", key: "stat", coords: "417,336,464,363", hint_text: "Stat", font_size: 10, depth: 0, font_family: "Arial, Helvetica, sans-serif"});
	area.push({type: "menu", shape: "rect", key: "timebase_ref", coords: "465,336,603,364", hint_text: "Timebase Ref (Int)", font_size: 10, depth: 0, font_family: "Arial, Helvetica, sans-serif"});
	area.push({type: "menu", shape: "rect", key: "misc", coords: "604,336,645,364", hint_text: "Misc", font_size: 10, depth: 0, font_family: "Arial, Helvetica, sans-serif"});
	this.set_fonts(area);
	return area;
}

MenuAreaBuilder.prototype.menu_input = function () {
	var area = [];
	area.push({type: "menu", shape: "rect", key: "drop_minus", coords: "127,330,161,348",  tooltip: "|_"});
	area.push({type: "menu", shape: "rect", key: "drop_plus", coords: "127,348,161,363", tooltip: "_|"});
	area.push({type: "menu", shape: "rect", key: "dc", coords: "190,330,228,347", tooltip: "DC"});
	area.push({type: "menu", shape: "rect", key: "ac", coords: "190,348,228,364", tooltip: "AC"});
	area.push({type: "menu", shape: "rect", key: "R1", coords: "264,330,301,347", tooltip: "1 МОм"});
	area.push({type: "menu", shape: "rect", key: "R50", coords: "264,348,301,363", tooltip: "50 Ом"});
	area.push({type: "menu", shape: "rect", key: "fading10", coords: "344,329,383,347", tooltip: "10x"});
	area.push({type: "menu", shape: "rect", key: "fading1", coords: "344,347,383,363", tooltip: "1x"});
	area.push({type: "menu", shape: "rect", key: "start_auto", coords: "420,330,465,347", tooltip: "Auto"});
	area.push({type: "menu", shape: "rect", key: "start_man", coords: "420,348,465,363", tooltip: "Man"});
	area.push({type: "menu", shape: "rect", key: "trig_input", coords: "486,330,550,364", tooltip: "Trig 0 V"});
	area.push({type: "menu", shape: "rect", key: "filter_input_a", coords: "571,331,641,363", tooltip: "Filter (Off)"});
	this.set_fonts(area);
	return area;
}

MenuAreaBuilder.prototype.input_filter = function () {
	var area = [];
	area.push({type: "menu", shape: "rect", key: "an_lp", coords: "124,333,255,363", tooltip: "Analog LP 100kHz Off"});
	area.push({type: "menu", shape: "rect", key: "dig_lp", coords: "305,333,437,363", tooltip: "Digital LP Off"});
	area.push({type: "menu", shape: "rect", key: "dig_lp_freq", coords: "489,334,628,363", tooltip: "Digital LP Freq 100kHz"});
	this.set_fonts(area);
	return area;
}

MenuAreaBuilder.prototype.lp_val = function () {
	var area = [];
	area.push({shape: "rect", key: "name_lp", coords: "139,315,269,333", hint_text: "dig:"});
	area.push({shape: "rect", key: "count_hz", coords: "270,315,305,333", hint_text: "0 KHz"});
	area.push({shape: "menu", key: "lp_hz", coords: "445,343,495,363", hint_text: "Hz"});
	area.push({shape: "menu", key: "lp_mhz", coords: "527,343,582,363", hint_text: "MHz"});
	this.set_fonts(area);
	return area;
}

MenuAreaBuilder.prototype.menu_set_burst = function () {
	var area = [];
	area.push({type: "menu", shape: "rect", key: "burst_sync_delay", coords: "108,336,241,364", hint_text: "Sync delay 200us"});
	area.push({type: "menu", shape: "rect", key: "burst_start_delay", coords: "243,336,373,363", hint_text: "Start delay 200us"});
	area.push({type: "menu", shape: "rect", key: "burst_meas_time", coords: "375,336,515,363", hint_text: "Meas Time 200us"});
	area.push({type: "menu", shape: "rect", key: "burst_freq_limit", coords: "517,336,645,363", hint_text: "Frequency Limit 300MHz"});
	this.set_fonts(area);
	return area;
}

MenuAreaBuilder.prototype.menu_set_arm = function () {
	var area = [];
	area.push({type: "menu", shape: "rect", key: "arm_start_chan_off", coords: "108,336,202,364", hint_text: "Start Chan Off"});
	area.push({type: "menu", shape: "rect", key: "arm_start_slope", coords: "203,336,289,363", hint_text: "Start Slope"});
	area.push({type: "menu", shape: "rect", key: "arm_start_arm_delay", coords: "290,336,449,363", hint_text: "Start Arm delay 200us"});
	area.push({type: "menu", shape: "rect", key: "arm_stop_chan_off", coords: "451,336,545,363", hint_text: "Stop Chan Off"});
	area.push({type: "menu", shape: "rect", key: "arm_stop_slope", coords: "547,336,645,363", hint_text: "Stop Slope"});
	this.set_fonts(area);
	return area;
}

MenuAreaBuilder.prototype.menu_set_trigger_hold = function () {
	var area = [];
	area.push({type: "menu", shape: "rect", key: "hold_off_off", coords: "156,336,319,364", hint_text: "Hold Off Off"});
	area.push({type: "menu", shape: "rect", key: "set_trigger_hold_off", coords: "420,336,581,363", hint_text: "Trigger hold off 200us"});
	this.set_fonts(area);
	return area;
}

MenuAreaBuilder.prototype.menu_set_stat = function () {
	var area = [];
	area.push({type: "menu", shape: "rect", key: "stat_sync_delay", coords: "108,336,241,364", hint_text: "No. Of Samples 100"});
	area.push({type: "menu", shape: "rect", key: "stat_start_delay", coords: "243,336,373,363", hint_text: "No. Of Bins 20"});
	area.push({type: "menu", shape: "rect", key: "stat_burst_meas_time", coords: "375,336,515,363", hint_text: "Pacing Off"});
	area.push({type: "menu", shape: "rect", key: "stat_freq_limit", coords: "517,336,645,363", hint_text: "Pacing Time 1s"});
	this.set_fonts(area);
	return area;
}

MenuAreaBuilder.prototype.menu_set_timebase = function () {
	var area = [];
	area.push({type: "menu", shape: "rect", key: "timebase_int", coords: "145,334,239,362", hint_text: "Int"});
	area.push({type: "menu", shape: "rect", key: "timebase_ext", coords: "309,335,395,362", hint_text: "Ext"});
	area.push({type: "menu", shape: "rect", key: "timebase_auto", coords: "461,336,562,363", hint_text: "Auto"});
	this.set_fonts(area);
	return area;
}

MenuAreaBuilder.prototype.menu_set_misc = function () {
	var area = [];
	area.push({type: "menu", shape: "rect", key: "misc_sync_delay", coords: "108,336,241,364", hint_text: "Smart Time Interval Off"});
	area.push({type: "menu", shape: "rect", key: "misc_start_delay", coords: "243,336,373,363", hint_text: "Smart Freq (Auto)"});
	area.push({type: "menu", shape: "rect", key: "misc_burst_meas_time", coords: "375,336,515,363", hint_text: "Auto Trig Low Freq 100Hz"});
	area.push({type: "menu", shape: "rect", key: "misc_freq_limit", coords: "517,336,645,363", hint_text: "Timeout (Off)"});
	this.set_fonts(area);
	return area;
}

MenuAreaBuilder.prototype.menu_math_limit = function () {
	var area = [];
	area.push({type: "menu", shape: "rect", key: "menu_math", coords: "238,330,313,362", tooltip: "Math"});
	area.push({type: "menu", shape: "rect", key: "menu_limit", coords: "379,331,461,363", tooltip: "Limits"});
	this.set_fonts(area);
	return area;
}

MenuAreaBuilder.prototype.math = function () {
	var area = [];
	area.push({type: "menu", shape: "rect", key: "math_math", coords: "145,332,227,364", tooltip: "Math (Off)"});
	area.push({type: "menu", shape: "rect", key: "math_k", coords: "269,331,351,363", tooltip: "K 1 E0"});
	area.push({type: "menu", shape: "rect", key: "math_l", coords: "395,331,471,363", tooltip: "L 0 E0"});
	area.push({type: "menu", shape: "rect", key: "math_m", coords: "519,332,596,364", tooltip: "M 1 E0"});
	this.set_fonts(area);
	return area;
}

MenuAreaBuilder.prototype.math_constant = function () {
	var area = [];
	area.push({shape: "rect", key: "constant", coords: "139,315,169,333", hint_text: "K:"});
	area.push({shape: "rect", key: "count_mantis", coords: "170,315,205,333", hint_text: "0"});
	area.push({shape: "rect", key: "const_ee", coords: "207,315,239,333", hint_text: "EE"});
	area.push({shape: "rect", key: "count_exp", coords: "241,315,278,333", hint_text: "0"});
	area.push({shape: "rect", key: "menu_ee", coords: "485,343,535,363", hint_text: "EE"});
	area.push({shape: "rect", key: "menu_const_ok", coords: "567,343,622,363", hint_text: "X0"});
	this.set_fonts(area);
	return area;
}


MenuAreaBuilder.prototype.math_math = function () {
	var area = [];
	area.push({type: "menu", shape: "rect", key: "math_math_off", coords: "111,331,186,363", tooltip: "Off"});
	area.push({type: "menu", shape: "rect", key: "math_func1", coords: "221,331,303,363", tooltip: "K*X+L"});
	area.push({type: "menu", shape: "rect", key: "math_func2", coords: "338,331,414,363", tooltip: "K/X+L"});
	area.push({type: "menu", shape: "rect", key: "math_func3", coords: "450,333,523,364", tooltip: "K*X/M+L"});
	area.push({type: "menu", shape: "rect", key: "math_func4", coords: "560,332,634,363", tooltip: "(K/X+L)/M"});
	this.set_fonts(area);
	return area;
}

MenuAreaBuilder.prototype.limits = function () {
	var area = [];
	area.push({type: "menu", shape: "rect", key: "limit_beh", coords: "109,330,235,362", tooltip: "Limit Behavior (Off)"});
	area.push({type: "menu", shape: "rect", key: "limit_mode", coords: "236,330,370,362", tooltip: "Limit Mode (Range)"});
	area.push({type: "menu", shape: "rect", key: "low_lim", coords: "371,330,506,362", tooltip: "Lower Limit 4 E3"});
	area.push({type: "menu", shape: "rect", key: "upp_lim", coords: "507,330,642,362", tooltip: "Upper Limit 6 E3"});
	this.set_fonts(area);
	return area;
}

MenuAreaBuilder.prototype.limits_state = function () {
    var area = [];
    area.push({ type: "menu", shape: "rect", key: "limit_beh_off", coords: "109,328,248,364", tooltip: "Off" });
    area.push({ type: "menu", shape: "rect", key: "limit_beh_capture", coords: "249,328,378,364", tooltip: "Capture" });
    area.push({ type: "menu", shape: "rect", key: "limit_beh_alarm", coords: "379,328,513,364", tooltip: "Alarm" });
    area.push({ type: "menu", shape: "rect", key: "limit_beh_alarm_stop", coords: "514,328,642,364", tooltip: "Alarm astop" });
    this.set_fonts(area);
    return area;
}

MenuAreaBuilder.prototype.limits_mode = function () {
    var area = [];
    area.push({ type: 'menu', shape: "rect", key: "limit_mode_above", coords: "118,336,248,364", hint_text: "Above" });
    area.push({ type: 'menu', shape: "rect", key: "limit_mode_below", coords: "285,336,424,364", hint_text: "Below" });
    area.push({ type: 'menu', shape: "rect", key: "limit_mode_range", coords: "464,336,608,364", hint_text: "Range" });
    this.set_fonts(area);
    return area;
}
