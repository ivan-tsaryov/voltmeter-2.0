function MenuElement(name, param) {
	MenuElement.superclass.constructor.call(this, name, param);
	this.param = param;
  this.active = false;
}
extend(MenuElement, IndicatorText);

IndicatorText.prototype.setMap = function (map) {
	IndicatorNumber.superclass.setMap.call(this, map);
	map.addListener('state_change', this, function() {
		var cond = this.param.cond;
		var state = map.state;
		var menu = map.menu;
    if (eval(cond))
			this.setText(eval(this.param.val));
		else
			this.render.hide();
	})
}

MenuElement.prototype.isActive = function () {
  return this.active;
}

MenuElement.prototype.drawControl = function () {
}

MenuElement.prototype.activate = function () {
  this.active = true;
  if(!this.map.state.power) {
    return;
  }
  if(this.render.node_rect == undefined) {
    coordinates = this.render.attr.coords.split(',');
    x1 = coordinates[0];
    y1 = coordinates[1];
    x2 = coordinates[2];
    y2 = coordinates[3];
    this.render.node_rect = this.map.paper.rect(x1, y1, x2 - x1, y2 - y1);
  }
  if(this.render.node != null && this.render.node_rect != undefined) {
    this.render.node.toFront();
    this.render.node.show();
    this.render.node_rect.show();
    this.render.node_rect.attr({fill: 'black', opacity: '1'});
    this.render.node.attr({fill: 'white', opacity: '1', stroke: 'white'});
  }
}

MenuElement.prototype.makeStroke = function () {
  this.render.node.attr({fill: 'black', opacity: '1', stroke: 'black'});
}

MenuElement.prototype.deactivate = function () {
  this.active = false;
  if(this.render.node != null && this.render.node_rect != undefined) {
    this.render.node.show();
    this.render.node_rect.show();
    this.render.node_rect.attr({fill: 'black', opacity: '0', stroke: 'none'});
    this.render.node.attr({fill: 'black', opacity: '1', stroke: 'none'});
  }
}
