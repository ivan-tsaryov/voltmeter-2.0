var GENERATOR = "Настройте сигнал во вкладке \"Входные сигналы\". Выберите стандартный сигнал или введите свой.";
var SWITCHER = "Установить тумблер 'СЕТЬ' в положение 'ВКЛ' <img class='button_img' id='switcher' src='training/switcher.png'>";
var BUTTON_UDC = "Нажмите кнопку для измерения постоянного напряжения <img class='button_img' id='button_Udc' src='training/button.png'>";
var BUTTON_UAC = "Нажмите кнопку для измерения переменного напряжения <img class='button_img' id='button_Uac' src='training/button.png'>";
var BUTTON_R = "Нажмите кнопку для измерения сопротивления <img class='button_img' id='button_R' src='training/button.png'>";
var BUTTON_IDC = "Нажмите кнопку для измерения постоянного тока <img class='button_img' id='button_Idc' src='training/button.png'>";
var BUTTON_IAC = "Нажмите кнопку для измерения переменного тока <img class='button_img' id='button_Iac' src='training/button.png'>";
var WIRE_UR = "Подключите кабель ко входному гнезду для измерения напряжений <img class='button_img' id='wire_UR' src='training/wire_UR.png'>";
var WIRE_0 = "Подключите кабель ко входному гнезду общего провода <img class='button_img' id='wire_0' src='training/wire_0.png'>";
var WIRE_I = "Подключите кабель ко входному гнезду для измерения токов <img class='button_img' id='wire_I' src='training/wire_I.png'>";


Device4.prototype.InitEx = function () {
	this.exercises = [
		{
			name: "Измерение постоянного напряжения",
			nodes: [],
			control: "Настроить генератор, Настроить генератор входного сигнала во вкладке «Сигналы». Выберите пункт 1 пункт. Включить прибор, Установить тумблер \"СЕТЬ\" в положение \"ВКЛ\". Выбрать род работы, Нажмите кнопку для измерения постоянного напряжения. Подключить кабель K1,Подключите кабель ко входному гнезду для измерения напряжений. Подключить кабель K2, Подключите кабель ко входному гнезду общего провода. Выключите прибор, Установить тумблер \"СЕТЬ\" в положение \"ВЫКЛ\"."
		},
		{
			name: "Измерение переменного напряжения",
			nodes: [],
			control: "Включить прибор, Установить тумблер \"СЕТЬ\" в положение \"ВКЛ\". Выбрать род работы, Нажмите кнопку для измерения переменного напряжения. Подключить кабель K1,Подключите кабель ко входному гнезду для измерения напряжений. Подключить кабель K2, Подключите кабель ко входному гнезду общего провода. Выключите прибор, Установить тумблер \"СЕТЬ\" в положение \"ВЫКЛ\"."
		},
		{
			name: "Измерение сопротивления",
			nodes: [],
			control: "Включить прибор, Установить тумблер \"СЕТЬ\" в положение \"ВКЛ\". Выбрать род работы, Нажмите кнопку для измерения сопротивления. Подключить кабель K1,Подключите кабель ко входному гнезду для измерения сопротивлений. Подключить кабель K2, Подключите кабель ко входному гнезду общего провода. Выключите прибор, Установить тумблер \"СЕТЬ\" в положение \"ВЫКЛ\"."
		},
		{
			name: "Измерение постоянного тока",
			nodes: [],
			control: "Включить прибор, Установить тумблер \"СЕТЬ\" в положение \"ВКЛ\". Выбрать род работы, Нажмите кнопку для измерения постоянного тока. Подключить кабель K1,Подключите кабель ко входному гнезду для измерения токов. Подключить кабель K2, Подключите кабель ко входному гнезду общего провода. Выключите прибор, Установить тумблер \"СЕТЬ\" в положение \"ВЫКЛ\"."
		},
		{
			name: "Измерение переменного тока",
			nodes: [],
			control: "Включить прибор, Установить тумблер \"СЕТЬ\" в положение \"ВКЛ\". Выбрать род работы, Нажмите кнопку для измерения переменного тока. Подключить кабель K3,Подключите кабель ко входному гнезду для измерения токов. Подключить кабель K2, Подключите кабель ко входному гнезду общего провода. Выключите прибор, Установить тумблер \"СЕТЬ\" в положение \"ВЫКЛ\"."
		}
	]
	this.Ex1();
	this.Ex2();
	this.Ex3();
	this.Ex4();
	this.Ex5();
};

Device4.prototype.get_hash = function () {
	var foo = this.state_time_variables;
	return hex_md5(print_object(this.tmpState, function (arg) { return $.inArray(arg, foo) != -1 }));
};

Device4.prototype.Ex1 = function () {
	//1
	this.tmpState = jQuery.extend(true, {}, this.state);

	this.tmpState.setSignal = true;
	this.tmpState.button_push = "radio";
	this.exercises[0].nodes.push({
		hash: this.get_hash(),
		goal: "Настроить генератор",
		comment: "Шаг первый. Настроить генератор входного сигнала во вкладке «Сигналы». Выберите пункт 1 пункт. ",
		button_hint: GENERATOR
	});

	//2
	this.tmpState.switcher = true;
	this.tmpState.button_push = "switcher";
	this.exercises[0].nodes.push({
		hash: this.get_hash(),
		goal: "Включить прибор",
		comment: "Шаг второй. Установить тумблер \"СЕТЬ\" в положение \"ВКЛ\"",
		button_hint: SWITCHER
	});
	//3
	this.tmpState.measurement = 1;
	this.tmpState.button_push = "button_Udc";
	this.exercises[0].nodes.push({
		hash: this.get_hash(),
		goal: "Выбрать род работы",
		comment: "Шаг третий. Нажмите кнопку для измерения постоянного напряжения",
		button_hint: BUTTON_UDC
	});
	//4
	this.tmpState.wire_UR = true;
	this.tmpState.button_push = "wire_UR";
	this.exercises[0].nodes.push({
		hash: this.get_hash(),
		goal: "Подключить кабель K1",
		comment: "Шаг четвертый. Подключите кабель ко входному гнезду для измерения напряжений",
		button_hint: WIRE_UR
	});
	//5
	this.tmpState.wire_0 = true;
	this.tmpState.button_push = "wire_0";
	this.exercises[0].nodes.push({
		hash: this.get_hash(),
		goal: "Подключить кабель K2",
		comment: "Шаг пятый. Подключите кабель ко входному гнезду общего провода",
		button_hint: WIRE_0
	});
	//6
	this.tmpState.switcher = false;
	this.tmpState.button_push = "switcher";
	this.exercises[0].nodes.push({
		hash: this.get_hash(),
		goal: "Выключить прибор",
		comment: "Шаг седьмой. Установить тумблер \"СЕТЬ\" в положение \"ВЫКЛ\".",
		button_hint: SWITCHER
	});
};

Device4.prototype.Ex2 = function () {
	//1
	this.tmpState = jQuery.extend(true, {}, this.state);

	this.tmpState.setSignal = true;
	this.tmpState.button_push = "radio";
	this.exercises[1].nodes.push({
		hash: this.get_hash(),
		goal: "Настроить генератор",
		comment: "Шаг первый. Настроить генератор входного сигнала во вкладке «Сигналы». Выберите пункт 1 пункт. ",
		button_hint: GENERATOR
	});

	//2
	this.tmpState.switcher = true;
	this.tmpState.button_push = "switcher";
	this.exercises[1].nodes.push({
		hash: this.get_hash(),
		goal: "Включить прибор",
		comment: "Шаг второй. Установить тумблер 'СЕТЬ' в положение 'ВКЛ'",
		button_hint: SWITCHER
	});
	//3
	this.tmpState.measurement = 2;
	this.tmpState.button_push = "button_Uac";
	this.exercises[1].nodes.push({
		hash: this.get_hash(),
		goal: "Выбрать род работы",
		comment: "Шаг третий. Нажмите кнопку для измерения переменного напряжения",
		button_hint: BUTTON_UAC
	});
	//4
	this.tmpState.wire_UR = true;
	this.tmpState.button_push = "wire_UR";
	this.exercises[1].nodes.push({
		hash: this.get_hash(),
		goal: "Подключить кабель K1",
		comment: "Шаг четвертый. Подключите кабель ко входному гнезду для измерения напряжений",
		button_hint: WIRE_UR
	});
	//5
	this.tmpState.wire_0 = true;
	this.tmpState.button_push = "wire_0";
	this.exercises[1].nodes.push({
		hash: this.get_hash(),
		goal: "Подключить кабель K2",
		comment: "Шаг пятый. Подключите кабель ко входному гнезду общего провода",
		button_hint: WIRE_0
	});
	//6
	this.tmpState.switcher = false;
	this.tmpState.button_push = "switcher";
	this.exercises[1].nodes.push({
		hash: this.get_hash(),
		goal: "Выключить прибор",
		comment: "Шаг седьмой. Установить тумблер 'СЕТЬ' в положение 'ВЫКЛ'.",
		button_hint: SWITCHER
	});
};

Device4.prototype.Ex3 = function () {
	//1
	this.tmpState = jQuery.extend(true, {}, this.state);

	this.tmpState.setSignal = true;
	this.tmpState.button_push = "radio";
	this.exercises[2].nodes.push({
		hash: this.get_hash(),
		goal: "Настроить генератор",
		comment: "Шаг первый. Настроить генератор входного сигнала во вкладке «Сигналы». Выберите пункт 1 пункт. ",
		button_hint: GENERATOR
	});

	//2
	this.tmpState.switcher = true;
	this.tmpState.button_push = "switcher";
	this.exercises[2].nodes.push({
		hash: this.get_hash(),
		goal: "Включить прибор",
		comment: "Шаг второй. Установить тумблер 'СЕТЬ' в положение 'ВКЛ'",
		button_hint: SWITCHER
	});
	//3
	this.tmpState.measurement = 3;
	this.tmpState.button_push = "button_R";
	this.exercises[2].nodes.push({
		hash: this.get_hash(),
		goal: "Выбрать род работы",
		comment: "Шаг третий. Нажмите кнопку для измерения сопротивления",
		button_hint: BUTTON_R
	});
	//4
	this.tmpState.wire_UR = true;
	this.tmpState.button_push = "wire_UR";
	this.exercises[2].nodes.push({
		hash: this.get_hash(),
		goal: "Подключить кабель K1",
		comment: "Шаг четвертый. Подключите кабель ко входному гнезду для измерения сопротивлений",
		button_hint: WIRE_UR
	});
	//5
	this.tmpState.wire_0 = true;
	this.tmpState.button_push = "wire_0";
	this.exercises[2].nodes.push({
		hash: this.get_hash(),
		goal: "Подключить кабель K2",
		comment: "Шаг пятый. Подключите кабель ко входному гнезду общего провода",
		button_hint: WIRE_0
	});
	//6
	this.tmpState.switcher = false;
	this.tmpState.button_push = "switcher";
	this.exercises[2].nodes.push({
		hash: this.get_hash(),
		goal: "Выключить прибор",
		comment: "Шаг седьмой. Установить тумблер 'СЕТЬ' в положение 'ВЫКЛ'.",
		button_hint: SWITCHER
	});
};

Device4.prototype.Ex4 = function () {

	this.tmpState = jQuery.extend(true, {}, this.state);
	//1

	this.tmpState.setSignal = true;
	this.tmpState.button_push = "radio";
	this.exercises[3].nodes.push({
		hash: this.get_hash(),
		goal: "Настроить генератор",
		comment: "Шаг первый. Настроить генератор входного сигнала во вкладке «Сигналы». Выберите пункт 1 пункт. ",
		button_hint: GENERATOR
	});

	//2
	this.tmpState.switcher = true;
	this.tmpState.button_push = "switcher";
	this.exercises[3].nodes.push({
		hash: this.get_hash(),
		goal: "Включить прибор",
		comment: "Шаг второй. Установить тумблер 'СЕТЬ' в положение 'ВКЛ'",
		button_hint: SWITCHER
	});
	//3
	this.tmpState.measurement = 4;
	this.tmpState.button_push = "button_Idc";
	this.exercises[3].nodes.push({
		hash: this.get_hash(),
		goal: "Выбрать род работы",
		comment: "Шаг третий. Нажмите кнопку для измерения постоянного тока",
		button_hint: BUTTON_IDC
	});
	//4
	this.tmpState.wire_I = true;
	this.tmpState.button_push = "wire_I";
	this.exercises[3].nodes.push({
		hash: this.get_hash(),
		goal: "Подключить кабель K3",
		comment: "Шаг четвертый. Подключите кабель ко входному гнезду для измерения токов",
		button_hint: WIRE_I
	});
	//5
	this.tmpState.wire_0 = true;
	this.tmpState.button_push = "wire_0";
	this.exercises[3].nodes.push({
		hash: this.get_hash(),
		goal: "Подключить кабель K2",
		comment: "Шаг пятый. Подключите кабель ко входному гнезду общего провода",
		button_hint: WIRE_0
	});
	//6
	this.tmpState.switcher = false;
	this.tmpState.button_push = "switcher";
	this.exercises[3].nodes.push({
		hash: this.get_hash(),
		goal: "Выключить прибор",
		comment: "Шаг седьмой. Установить тумблер 'СЕТЬ' в положение 'ВЫКЛ'.",
		button_hint: SWITCHER
	});
};

Device4.prototype.Ex5 = function () {
	this.tmpState = jQuery.extend(true, {}, this.state);
	//1

	this.tmpState.setSignal = true;
	this.tmpState.button_push = "radio";
	this.exercises[4].nodes.push({
		hash: this.get_hash(),
		goal: "Настроить генератор",
		comment: "Шаг первый. Настроить генератор входного сигнала во вкладке «Сигналы». Выберите пункт 1 пункт. ",
		button_hint: GENERATOR
	});

	//2
	this.tmpState.switcher = true;
	this.tmpState.button_push = "switcher";
	this.exercises[4].nodes.push({
		hash: this.get_hash(),
		goal: "Включить прибор",
		comment: "Шаг второй. Установить тумблер 'СЕТЬ' в положение 'ВКЛ'",
		button_hint: SWITCHER
	});
	//3
	this.tmpState.measurement = 5;
	this.tmpState.button_push = "button_Iac";
	this.exercises[4].nodes.push({
		hash: this.get_hash(),
		goal: "Выбрать род работы",
		comment: "Шаг третий. Нажмите кнопку для измерения переменного тока",
		button_hint: BUTTON_IAC
	});
	//4
	this.tmpState.wire_I = true;
	this.tmpState.button_push = "wire_I";
	this.exercises[4].nodes.push({
		hash: this.get_hash(),
		goal: "Подключить кабель K3",
		comment: "Шаг четвертый. Подключите кабель ко входному гнезду для измерения токов",
		button_hint: WIRE_I
	});
	//5
	this.tmpState.wire_0 = true;
	this.tmpState.button_push = "wire_0";
	this.exercises[4].nodes.push({
		hash: this.get_hash(),
		goal: "Подключить кабель K2",
		comment: "Шаг пятый. Подключите кабель ко входному гнезду общего провода",
		button_hint: WIRE_0
	});
	//6
	this.tmpState.switcher = false;
	this.tmpState.button_push = "switcher";
	this.exercises[4].nodes.push({
		hash: this.get_hash(),
		goal: "Выключить прибор",
		comment: "Шаг седьмой. Установить тумблер 'СЕТЬ' в положение 'ВЫКЛ'.",
		button_hint: SWITCHER
	});
};